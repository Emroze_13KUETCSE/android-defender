package com.example.hungers.androiddefender;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class change_pin extends AppCompatActivity {
    public static String PREFS_NAME="hungers";
    Button ok;
    EditText old_pin,new_pin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pin);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();
        ok=(Button)findViewById(R.id.btn_ok1);
        old_pin=(EditText) findViewById(R.id.et_pin1);
        new_pin=(EditText) findViewById(R.id.et_pin2);

        ok.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ok.getBackground().setAlpha(100);
                        break;
                    case MotionEvent.ACTION_UP:
                        ok.getBackground().setAlpha(255);
                        if (old_pin.getText().toString().equals("") || new_pin.getText().toString().equals("")  ) {
                            LayoutInflater inflater = getLayoutInflater();
                            View layout = inflater.inflate(R.layout.toast,
                                    (ViewGroup) findViewById(R.id.toast_layout_root));

                            ImageView image = (ImageView) layout.findViewById(R.id.image);
                            image.setImageResource(R.drawable.logo);
                            TextView text = (TextView) layout.findViewById(R.id.text);
                            text.setText("Text fields are empty!!!");

                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.BOTTOM, 0, 110);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();
                            Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib.vibrate(500);
                        }
                        else {
                            if(new_pin.getText().toString().length() > 30){
                                Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vib.vibrate(500);
                                new_pin.setText("");

                                LayoutInflater inflater = getLayoutInflater();
                                View layout = inflater.inflate(R.layout.toast,
                                        (ViewGroup) findViewById(R.id.toast_layout_root));

                                ImageView image = (ImageView) layout.findViewById(R.id.image);
                                image.setImageResource(R.drawable.logo);
                                TextView text = (TextView) layout.findViewById(R.id.text);
                                text.setText("PIN must be less than 30 digit!!!");

                                Toast toast = new Toast(getApplicationContext());
                                toast.setGravity(Gravity.BOTTOM, 0, 110);
                                toast.setDuration(Toast.LENGTH_LONG);
                                toast.setView(layout);
                                toast.show();
                            }
                            else {
                                if (!preference.getString("pin", null).equals(old_pin.getText().toString())) {

                                    old_pin.setText("");
                                    new_pin.setText("");

                                    Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                    vib.vibrate(500);
                                    LayoutInflater inflater = getLayoutInflater();
                                    View layout = inflater.inflate(R.layout.toast,
                                            (ViewGroup) findViewById(R.id.toast_layout_root));

                                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                                    image.setImageResource(R.drawable.logo);
                                    TextView text = (TextView) layout.findViewById(R.id.text);
                                    text.setText("PIN mis-matched");

                                    Toast toast = new Toast(getApplicationContext());
                                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                                    toast.setDuration(Toast.LENGTH_LONG);
                                    toast.setView(layout);
                                    toast.show();
                                } else if (preference.getString("pin", null).equals(old_pin.getText().toString())) {
                                    editor.putString("pin", new_pin.getText().toString());
                                    editor.commit();
                                    LayoutInflater inflater = getLayoutInflater();
                                    View layout = inflater.inflate(R.layout.toast,
                                            (ViewGroup) findViewById(R.id.toast_layout_root));

                                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                                    image.setImageResource(R.drawable.logo);
                                    TextView text = (TextView) layout.findViewById(R.id.text);
                                    text.setText("PIN has been changed");

                                    Toast toast = new Toast(getApplicationContext());
                                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                                    toast.setDuration(Toast.LENGTH_LONG);
                                    toast.setView(layout);
                                    toast.show();
                                    finish();
                                }
                            }
                        }

                        break;
                }
                return false;
            }
        });

    }
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();



    }
}
