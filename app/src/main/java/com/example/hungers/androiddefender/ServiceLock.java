package com.example.hungers.androiddefender;


import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.TextClock;


public class ServiceLock extends Service {
    private WindowManager wm;
    private LinearLayout ll1;
    public static String PREFS_NAME="hungers";
    private android.widget.Button ok;
    TextClock tc;
    ImageView iv;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        RemoteViews remoteViews = new RemoteViews(getPackageName(),
                R.layout.widget);
        android.support.v4.app.NotificationCompat.Builder mBuilder = new android.support.v4.app.NotificationCompat.Builder(
                this).setSmallIcon(R.drawable.notific).setContent(
                remoteViews);

        Intent resultIntent = new Intent(this, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        stackBuilder.addParentStack(MainActivity.class);

        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.button1, resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        startForeground(999, mBuilder.build());

    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        ll1 = new LinearLayout(this);
        ok = new Button(this);
        tc = new TextClock(this);
        iv= new ImageView(this);

        final WindowManager.LayoutParams parameters = new WindowManager.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,WindowManager.LayoutParams.FLAG_FULLSCREEN, PixelFormat.TRANSLUCENT);
        parameters.gravity = Gravity.CENTER;


        LinearLayout.LayoutParams llParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
        ll1.setBackgroundResource(R.drawable.rec1); //or whatever your image is
        ll1.setGravity(Gravity.CENTER);
        ll1.setOrientation(LinearLayout.VERTICAL);
        ll1.setLayoutParams(llParameters);


        LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT ,150 );
        params3.setMargins(50, 40, 50, 0);
        iv.setLayoutParams(params3);
        iv.setBackgroundResource(R.drawable.back_logo);

        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT ,WindowManager.LayoutParams.WRAP_CONTENT );
        params1.setMargins(50, 50, 50, 10);
        tc.setLayoutParams(params1);
        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/font11.ttf");
        tc.setTypeface(type);
        tc.setGravity(Gravity.CENTER);
        tc.setTextSize(75);
        tc.setTextColor(Color.argb(255, 0, 166, 225));


        ViewGroup.LayoutParams btn = new ViewGroup.LayoutParams(265,265);


        ok.setBackgroundResource(R.drawable.scr_lock);
        ok.setLayoutParams(btn);

        wm.addView(ll1,parameters);
        ll1.addView(iv);
        ll1.addView(tc);
        ll1.addView(ok);


        ok.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ok.getBackground().setAlpha(255);
                        break;
                    case MotionEvent.ACTION_UP:
                        ok.getBackground().setAlpha(255);

                        wm.removeView(ll1);

                        //if this view is removed then call a user then a error arised
                        editor.putString("c","2");
                        editor.commit();
                        break;
                }
                return false;
            }
        });


        ok.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                DevicePolicyManager DPM;
                ComponentName NM;


                DPM=(DevicePolicyManager)getApplication().getSystemService(Context.DEVICE_POLICY_SERVICE);
                NM=new ComponentName(getApplication(),Admin.class);
                DPM.setPasswordQuality(NM,DevicePolicyManager.PASSWORD_QUALITY_SOMETHING);
                DPM.lockNow();


                return false;
            }
        });

// listen the events get fired during the call
        StateListener phoneStateListener = new StateListener();
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        telephonyManager.listen(phoneStateListener,
                PhoneStateListener.LISTEN_CALL_STATE);


        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private class StateListener extends PhoneStateListener {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
            final SharedPreferences.Editor editor=preference.edit();

            super.onCallStateChanged(state, incomingNumber);
            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING:

                   if(/*preference.getString("lock","").equals("true") &&*/ preference.getString("c","").equals("1")){

                       new Handler().postDelayed(new Runnable() {
                           @Override
                           public void run() {
                               wm.removeView(ll1);
                               editor.putString("c","2");
                               editor.putString("ring","1");
                               editor.commit();
                           }
                       }, 1500);
                   }

                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:

                    break;
                case TelephonyManager.CALL_STATE_IDLE:

                    if(preference.getString("ring","").equals("1")){
                        editor.putString("ring","2");
                        editor.commit();
                    }

                    break;
            }
        }
    }
}
