package com.example.hungers.androiddefender;

import android.app.WallpaperManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import static android.R.id.edit;

public class Settings_lockScr extends AppCompatActivity {
    private static final int SELECT_PHOTO = 100;
    public static String PREFS_NAME="hungers";
    public ImageView iv;
    public Bitmap yourSelectedImage;
    SwitchCompat s8,s9;
    public final static int REQUEST_CODE = 10101;

    int i=0;

    public DevicePolicyManager DPM;
    public ComponentName NM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_lock_scr);




        Button btn = (Button) findViewById(R.id.btn_lc);
        Button btn1 = (Button) findViewById(R.id.btn_pre);
        Button btn2 = (Button) findViewById(R.id.btn_wall);
        Button btn3 = (Button) findViewById(R.id.btn_pass);

        iv = (ImageView) findViewById(R.id.iv_lc);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
            }
        });

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        String previouslyEncodedImage = preference.getString("image_data", "");

        if( !previouslyEncodedImage.equalsIgnoreCase("") ){
            byte[] b = Base64.decode(previouslyEncodedImage, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
            iv.setImageBitmap(bitmap);
        }


        //for switch
        s8 =(SwitchCompat) findViewById(R.id.s2);

        if(preference.getString("lock_scren",null)==null){
            editor.putString("lock_scren", "on");
            editor.commit();
        }

        if(preference.getString("lock_scren",null).equals("off")){
            s8.setChecked(false);
        } else{
            s8.setChecked(true);
        }

        if(s8.isChecked()){
            editor.putString("lock_scren", "on");
            editor.commit();
        }
        else{
            editor.putString("lock_scren", "off");
            editor.commit();
        }

        s8.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if(!isChecked){

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Lock Screen OFF");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("lock_scren", "off");
                    editor.commit();
                    stopService(new Intent(getApplicationContext(),LockScr.class));

                }
                else{



                    if (android.os.Build.VERSION.SDK_INT >= 23) {
                        if (!Settings.canDrawOverlays(getApplicationContext())) {
                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                    Uri.parse("package:" + getPackageName()));
                            startActivityForResult(intent, REQUEST_CODE);
                        }
                        else{
                            LayoutInflater inflater = getLayoutInflater();
                            View layout = inflater.inflate(R.layout.toast,
                                    (ViewGroup) findViewById(R.id.toast_layout_root));

                            ImageView image = (ImageView) layout.findViewById(R.id.image);
                            image.setImageResource(R.drawable.logo);
                            TextView text = (TextView) layout.findViewById(R.id.text);
                            text.setText("Lock Screen ON");

                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.BOTTOM, 0, 110);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();

                            editor.putString("lock_scren", "on");
                            editor.commit();

                        }
                    }
                    else{
                        LayoutInflater inflater = getLayoutInflater();
                        View layout = inflater.inflate(R.layout.toast,
                                (ViewGroup) findViewById(R.id.toast_layout_root));

                        ImageView image = (ImageView) layout.findViewById(R.id.image);
                        image.setImageResource(R.drawable.logo);
                        TextView text = (TextView) layout.findViewById(R.id.text);
                        text.setText("Lock Screen ON");

                        Toast toast = new Toast(getApplicationContext());
                        toast.setGravity(Gravity.BOTTOM, 0, 110);
                        toast.setDuration(Toast.LENGTH_LONG);
                        toast.setView(layout);
                        toast.show();

                        editor.putString("lock_scren", "on");
                        editor.commit();


                    }

                }

            }
        });

        s9 =(SwitchCompat) findViewById(R.id.s3);

        if(preference.getString("call_security",null)==null){
            editor.putString("call_security", "on");
            editor.commit();
        }

        if(preference.getString("call_security",null).equals("off")){
            s9.setChecked(false);
        } else{
            s9.setChecked(true);
        }

        if(s9.isChecked()){
            editor.putString("call_security", "on");
            editor.commit();
        }
        else{
            editor.putString("call_security", "off");
            editor.commit();
        }

        s9.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if(!isChecked){

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Call Security OFF");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("call_security", "off");
                    editor.commit();


                }
                else{


                        LayoutInflater inflater = getLayoutInflater();
                        View layout = inflater.inflate(R.layout.toast,
                                (ViewGroup) findViewById(R.id.toast_layout_root));

                        ImageView image = (ImageView) layout.findViewById(R.id.image);
                        image.setImageResource(R.drawable.logo);
                        TextView text = (TextView) layout.findViewById(R.id.text);
                        text.setText("Call Security ON");

                        Toast toast = new Toast(getApplicationContext());
                        toast.setGravity(Gravity.BOTTOM, 0, 110);
                        toast.setDuration(Toast.LENGTH_LONG);
                        toast.setView(layout);
                        toast.show();

                        editor.putString("call_security", "on");
                        editor.commit();




                }

            }
        });

//////////////////////////////////////////////////////////////////////////////////

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startService(new Intent(getApplicationContext(),LockScr.class));
                i=1;
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                DPM=(DevicePolicyManager)getSystemService(Context.DEVICE_POLICY_SERVICE);
                NM=new ComponentName(getApplicationContext(),Admin.class);

                /*DPM.removeActiveAdmin(NM);

                if(!DPM.isAdminActive(NM))
                {
                    Intent intent=new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                    intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,NM);
                    intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "To Take Security Measures");
                    startActivity(intent);
                }*/

                DPM.setPasswordMinimumLength(NM, 0);
                DPM.resetPassword("", DevicePolicyManager.RESET_PASSWORD_REQUIRE_ENTRY);
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast,
                        (ViewGroup) findViewById(R.id.toast_layout_root));

                ImageView image = (ImageView) layout.findViewById(R.id.image);
                image.setImageResource(R.drawable.logo);
                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText("System Password has been removed");

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.BOTTOM, 0, 110);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();


            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                WallpaperManager wallpaperManager = WallpaperManager.getInstance(getApplicationContext());
                Drawable wallpaperDrawable = wallpaperManager.getDrawable();
                iv.setImageDrawable(wallpaperDrawable);

                yourSelectedImage = ((BitmapDrawable)wallpaperDrawable).getBitmap();
                DisplayMetrics dm = getResources().getDisplayMetrics();

                yourSelectedImage = scaleCenterCrop(yourSelectedImage,dm.heightPixels,dm.widthPixels);


                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                yourSelectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();

                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

                final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                final SharedPreferences.Editor editor=preference.edit();

                editor.putString("image_data",encodedImage);
                editor.commit();

                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast,
                        (ViewGroup) findViewById(R.id.toast_layout_root));

                ImageView image = (ImageView) layout.findViewById(R.id.image);
                image.setImageResource(R.drawable.logo);
                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText("Default Wallpaper has been set");

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.BOTTOM, 0, 110);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if (requestCode == REQUEST_CODE) {
            if (android.os.Build.VERSION.SDK_INT >= 23) {
                if (Settings.canDrawOverlays(this)) {

                    editor.putString("lock_scren", "on");
                    editor.commit();

                    s8.setChecked(true);
                }
            }
        }

        switch(requestCode) {
            case SELECT_PHOTO:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    InputStream imageStream = null;
                    try {
                        imageStream = getContentResolver().openInputStream(selectedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    yourSelectedImage = BitmapFactory.decodeStream(imageStream);

                    DisplayMetrics dm = getResources().getDisplayMetrics();

                    yourSelectedImage = scaleCenterCrop(yourSelectedImage,dm.heightPixels,dm.widthPixels);

                    iv.setImageBitmap(yourSelectedImage);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    yourSelectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] b = baos.toByteArray();

                    String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);


                    editor.putString("image_data",encodedImage);
                    editor.commit();
                }
        }
    }
    public Bitmap scaleCenterCrop(Bitmap source, int newHeight, int newWidth) {
        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();

        // Compute the scaling factors to fit the new height and width, respectively.
        // To cover the final image, the final scaling will be the bigger
        // of these two.
        float xScale = (float) newWidth / sourceWidth;
        float yScale = (float) newHeight / sourceHeight;
        float scale = Math.max(xScale, yScale);

        // Now get the size of the source bitmap when scaled
        float scaledWidth = scale * sourceWidth;
        float scaledHeight = scale * sourceHeight;

        // Let's find out the upper left coordinates if the scaled bitmap
        // should be centered in the new size give by the parameters
        float left = (newWidth - scaledWidth) / 2;
        float top = (newHeight - scaledHeight) / 2;

        // The target rectangle for the new, scaled version of the source bitmap will now
        // be
        RectF targetRect = new RectF(left, top, left + scaledWidth, top + scaledHeight);

        // Finally, we create a new bitmap of the specified size and draw our new,
        // scaled bitmap onto it.
        Bitmap dest = Bitmap.createBitmap(newWidth, newHeight, source.getConfig());
        Canvas canvas = new Canvas(dest);
        canvas.drawBitmap(source, null, targetRect, null);

        return dest;
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            if(i == 1){
                LockScr.wm.removeView(LockScr.ll2);
                i = 0;
            }
            else{
                finish();
                i=0;
            }

            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
