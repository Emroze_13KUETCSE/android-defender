package com.example.hungers.androiddefender;

import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class help extends AppCompatActivity {
    public static String PREFS_NAME="hungers";
    TextView t1,t2,t3,t4,t7,t8,t9,t10;
    LinearLayout l1,l2,l4,l5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        t1=(TextView) findViewById(R.id.tv_about);
        t2=(TextView) findViewById(R.id.tv_ad);
        t3=(TextView) findViewById(R.id.tv_rebootsms);
        t4=(TextView) findViewById(R.id.tv_rsd);

        t7=(TextView) findViewById(R.id.tv_dualsim);
        t8=(TextView) findViewById(R.id.tv_dsd);
        t9=(TextView) findViewById(R.id.tv_gps);
        t10=(TextView) findViewById(R.id.tv_gpsd);


        l1=(LinearLayout) findViewById(R.id.ll1);
        l2=(LinearLayout) findViewById(R.id.ll2);

        l4=(LinearLayout) findViewById(R.id.ll4);
        l5=(LinearLayout) findViewById(R.id.ll5);



        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/neo_bold.ttf");
        t1.setTypeface(type);
        t3.setTypeface(type);

        t7.setTypeface(type);
        t9.setTypeface(type);


        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        t2.setTypeface(type1);
        t4.setTypeface(type1);

        t8.setTypeface(type1);
        t10.setTypeface(type1);



        l1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(help.this,about_app.class);
                startActivity(i);
            }
        });
        l2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(help.this,switch_off.class);
                startActivity(i);
            }
        });
        l4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent4 = new Intent();
                intent4.setComponent(new ComponentName("com.android.settings","com.android.settings.Settings"));
                startActivity(intent4);
            }
        });



    }
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


    }
}
