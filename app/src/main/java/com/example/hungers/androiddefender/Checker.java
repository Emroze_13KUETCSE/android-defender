package com.example.hungers.androiddefender;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import javax.crypto.spec.SecretKeySpec;

/**
 * Created by emroz on 1/8/2017.
 */

public class Checker{

    protected SecretKeySpec sks;
    protected Context context;
    protected final String[] preKeyNameX = {"pKey1X","pKey2X","pKey3X"},preKeyNameY = {"pKey1Y","pKey2Y","pKey3Y"};
    protected final String[] keyNameX = {"Key1X","Key2X","Key3X"},keyNameY = {"Key1Y","Key2Y","Key3Y"};
    protected final String fileName = "Security_Key";
    private final int ERROR_MARGIN = 50;

    public Checker(){}

    public Checker(SecretKeySpec sks,Context context){
        this.sks = sks;
        this.context = context;
    }

    public boolean checkX(int position,int index){
        int x;
        Helper helper = new Helper();
        SharedPreferences security = context.getSharedPreferences("Security_Key", Context.MODE_PRIVATE);
        byte[] encodedBytes = Base64.decode(security.getString(preKeyNameX[index],"0"),Base64.DEFAULT);

        x = helper.decodeData(encodedBytes,sks);
        x = position-x;
        if(x < 0)
            x=-x;
        if(x<= ERROR_MARGIN)
            return true;
        else
            return false;
    }

    public boolean checkY(int position,int index){
        int y;
        Helper helper = new Helper();
        SharedPreferences security = context.getSharedPreferences(fileName,Context.MODE_PRIVATE);
        byte[] encodedBytes = Base64.decode(security.getString(preKeyNameY[index],"0"),Base64.DEFAULT);


        y = helper.decodeData(encodedBytes,sks);
        y = position-y;
        if(y < 0)
            y=-y;
        if(y<= ERROR_MARGIN)
            return true;
        else
            return false;

    }

    public boolean checkFinalX(int position,int index){
        int x;
        Helper helper = new Helper();
        SharedPreferences security = context.getSharedPreferences("Security_Key", Context.MODE_PRIVATE);
        byte[] encodedBytes = Base64.decode(security.getString(keyNameX[index],"0"),Base64.DEFAULT);

        x = helper.decodeData(encodedBytes,sks);
        x = position-x;
        if(x < 0)
            x=-x;
        if(x<= ERROR_MARGIN)
            return true;
        else
            return false;
    }

    public boolean checkFinalY(int position,int index){
        int y;
        Helper helper = new Helper();
        SharedPreferences security = context.getSharedPreferences(fileName,Context.MODE_PRIVATE);
        byte[] encodedBytes = Base64.decode(security.getString(keyNameY[index],"0"),Base64.DEFAULT);


        y = helper.decodeData(encodedBytes,sks);
        y = position-y;
        if(y < 0)
            y=-y;
        if(y<= ERROR_MARGIN)
            return true;
        else
            return false;

    }
}

