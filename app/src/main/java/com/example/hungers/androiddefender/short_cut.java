package com.example.hungers.androiddefender;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class short_cut extends Activity {
    public static String PREFS_NAME="hungers";

    public DevicePolicyManager DPM;
    public ComponentName NM;

    Camera camera,camera1;
    Camera cam = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_short_cut);
    }

    @Override
    protected void onResume() {
        super.onResume();

        try{
            torch_floatingWindow.wm.removeView(torch_floatingWindow.ll);
        }
        catch (NullPointerException e){

        }


        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        DisplayMetrics dm = getResources().getDisplayMetrics();

        double density = dm.density * 160;
        double x = Math.pow(dm.widthPixels / density, 2);
        double y = Math.pow(dm.heightPixels / density, 2);

        int iv_d;

        if(dm.heightPixels>=2000){
            iv_d = 150;
        }
        else if(dm.heightPixels>=1920 && dm.heightPixels<2000){
            iv_d = 130;
        }
        else if(dm.heightPixels >1280 && dm.heightPixels<1920){
            iv_d = 110;
        }
        else if(dm.heightPixels>=1000 && dm.heightPixels<=1280){
            iv_d = 80;
        }
        else{
            iv_d = 52;
        }

        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                iv_d ,iv_d);

        final LinearLayout lls=(LinearLayout) findViewById(R.id.lliv_ls);
        final LinearLayout llSMS=(LinearLayout) findViewById(R.id.lliv_so);
        final LinearLayout llMENU=(LinearLayout) findViewById(R.id.lliv_m);
        final LinearLayout llSTOP=(LinearLayout) findViewById(R.id.lliv_s);
        final LinearLayout llLOCK=(LinearLayout) findViewById(R.id.lliv_l);
        final LinearLayout llLOCATION=(LinearLayout) findViewById(R.id.lliv_loc);
        final LinearLayout llRPASS=(LinearLayout) findViewById(R.id.lliv_rp);
        final LinearLayout llCAM=(LinearLayout) findViewById(R.id.lliv_c);
        final LinearLayout llWIPE = (LinearLayout) findViewById(R.id.lliv_wd);

        final ImageView ivMENU = new ImageView(this);
        final ImageView ivLOCK = new ImageView(this);
        final ImageView ivSTOP = new ImageView(this);

        ivMENU.setLayoutParams(params1);
        ivMENU.setBackgroundResource(R.drawable.s_3);

        ivLOCK.setLayoutParams(params1);
        ivLOCK.setBackgroundResource(R.drawable.s_5);

        ivSTOP.setLayoutParams(params1);
        ivSTOP.setBackgroundResource(R.drawable.s_home);

        llLOCK.addView(ivLOCK);

        llSTOP.addView(ivSTOP);

        llMENU.addView(ivMENU);



        llLOCK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

                DPM = (DevicePolicyManager) getApplication().getSystemService(Context.DEVICE_POLICY_SERVICE);
                NM = new ComponentName(getApplication(), Admin.class);
                DPM.setPasswordQuality(NM, DevicePolicyManager.PASSWORD_QUALITY_SOMETHING);
                DPM.lockNow();

            }
        });

        llMENU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent i = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(i);
            }
        });

        llSTOP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent startMain = new Intent(Intent.ACTION_MAIN);
                startMain.addCategory(Intent.CATEGORY_HOME);
                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startMain);
            }
        });

        FrameLayout f = (FrameLayout) findViewById(R.id.fl1);

        f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final ImageView ivs = new ImageView(this);
        final ImageView ivs_e= new ImageView(this);

        final ImageView ivSMS = new ImageView(this);
        final ImageView ivSMS_e= new ImageView(this);

        final ImageView ivLOC = new ImageView(this);
        final ImageView ivLOC_e= new ImageView(this);

        final ImageView ivRP = new ImageView(this);
        final ImageView ivRP_e= new ImageView(this);

        final ImageView ivEC = new ImageView(this);
        final ImageView ivEC_e= new ImageView(this);

        final ImageView ivWD = new ImageView(this);
        final ImageView ivWD_e= new ImageView(this);




        ivs_e.setLayoutParams(params1);
        ivs_e.setBackgroundResource(R.drawable.s_4_e);
        ivs.setLayoutParams(params1);
        ivs.setBackgroundResource(R.drawable.s_4);

        if(preference.getString("lock_scren","").equals("on")){
            lls.addView(ivs_e);

        }
        else{
            lls.addView(ivs);

        }

        ivs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lls.removeView(ivs);
                lls.addView(ivs_e);
                editor.putString("lock_scren","on");
                editor.commit();

                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast,
                        (ViewGroup) findViewById(R.id.toast_layout_root));

                ImageView image = (ImageView) layout.findViewById(R.id.image);
                image.setImageResource(R.drawable.logo);
                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText("Lock screen ON");

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.BOTTOM, 0, 110);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
            }
        });

        ivs_e.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lls.addView(ivs);
                lls.removeView(ivs_e);
                editor.putString("lock_scren","off");
                editor.commit();

                stopService(new Intent(getApplicationContext(),LockScr.class));

                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast,
                        (ViewGroup) findViewById(R.id.toast_layout_root));

                ImageView image = (ImageView) layout.findViewById(R.id.image);
                image.setImageResource(R.drawable.logo);
                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText("Lock screen OFF");

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.BOTTOM, 0, 110);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
            }
        });

        //SMS


        ivSMS_e.setLayoutParams(params1);
        ivSMS_e.setBackgroundResource(R.drawable.s_2_e);
        ivSMS.setLayoutParams(params1);
        ivSMS.setBackgroundResource(R.drawable.s_2);

        if(preference.getString("rebootSMS","").equals("on")){
            llSMS.addView(ivSMS_e);


        }
        else{
            llSMS.addView(ivSMS);

        }

        ivSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llSMS.removeView(ivSMS);
                llSMS.addView(ivSMS_e);

                editor.putString("rebootSMS","on");
                editor.commit();

                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast,
                        (ViewGroup) findViewById(R.id.toast_layout_root));

                ImageView image = (ImageView) layout.findViewById(R.id.image);
                image.setImageResource(R.drawable.logo);
                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText("After Reboot SMS ON");

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.BOTTOM, 0, 110);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
            }
        });

        ivSMS_e.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llSMS.addView(ivSMS);
                llSMS.removeView(ivSMS_e);

                editor.putString("rebootSMS","off");
                editor.commit();

                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast,
                        (ViewGroup) findViewById(R.id.toast_layout_root));

                ImageView image = (ImageView) layout.findViewById(R.id.image);
                image.setImageResource(R.drawable.logo);
                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText("After Reboot SMS OFF");

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.BOTTOM, 0, 110);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
            }
        });

        //LOCATION



        ivLOC_e.setLayoutParams(params1);
        ivLOC_e.setBackgroundResource(R.drawable.s_7_e);
        ivLOC.setLayoutParams(params1);
        ivLOC.setBackgroundResource(R.drawable.s_7);


        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if ( manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            //enable
            llLOCATION.addView(ivLOC_e);
            llLOCATION.removeView(ivLOC);
        }
        else{
            //not enable
            llLOCATION.addView(ivLOC);
            llLOCATION.removeView(ivLOC_e);
        }


        ivLOC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
                finish();
            }
        });

        ivLOC_e.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
                finish();
            }
        });

        //Torch




        ivRP.setLayoutParams(params1);
        ivRP.setBackgroundResource(R.drawable.recent);
        llRPASS.addView(ivRP);

        /*if(preference.getString("cam","").equals("off")){

        }
        else {
            llRPASS.addView(ivRP_e);
        }*/


        ivRP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

                Class serviceManagerClass = null;
                try {
                    serviceManagerClass = Class.forName("android.os.ServiceManager");
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                Method getService = null;
                try {
                    getService = serviceManagerClass.getMethod("getService", String.class);
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
                IBinder retbinder = null;
                try {
                    retbinder = (IBinder) getService.invoke(serviceManagerClass, "statusbar");
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                Class statusBarClass = null;
                try {
                    statusBarClass = Class.forName(retbinder.getInterfaceDescriptor());
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                Object statusBarObject = null;
                try {
                    statusBarObject = statusBarClass.getClasses()[0].getMethod("asInterface", IBinder.class).invoke(null, new Object[] { retbinder });
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
                Method clearAll = null;
                try {
                    clearAll = statusBarClass.getMethod("toggleRecentApps");
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
                clearAll.setAccessible(true);
                try {
                    clearAll.invoke(statusBarObject);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }

                /*llRPASS.removeView(ivRP);
                llRPASS.addView(ivRP_e);

                editor.putString("cam","on");
                editor.commit();*/


/*
                if (Build.VERSION.SDK_INT >= 20) {

                   //flashOn_l();
                }
                else{
                    flashOn();
                }*/
                /*Camera.Parameters p = camera.getParameters();
                p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                camera.setParameters(p);
                camera.startPreview();*/

            }
        });

        /*ivRP_e.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llRPASS.addView(ivRP);
                llRPASS.removeView(ivRP_e);

                editor.putString("cam","off");
                editor.commit();
                if (Build.VERSION.SDK_INT >= 20) {
                    //turnOffFlashLight();
                }
                else{
                    flashOff();

                }



            }
        });*/

        //EC



        ivEC_e.setLayoutParams(params1);
        ivEC_e.setBackgroundResource(R.drawable.call_sec_e);
        ivEC.setLayoutParams(params1);
        ivEC.setBackgroundResource(R.drawable.call_sec);

        if(preference.getString("call_security","").equals("on")){
            llCAM.addView(ivEC_e);

        }
        else{
            llCAM.addView(ivEC);

        }

        ivEC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llCAM.removeView(ivEC);
                llCAM.addView(ivEC_e);
                editor.putString("call_security","on");
                editor.commit();

                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast,
                        (ViewGroup) findViewById(R.id.toast_layout_root));

                ImageView image = (ImageView) layout.findViewById(R.id.image);
                image.setImageResource(R.drawable.logo);
                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText("Call Security ON");

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.BOTTOM, 0, 110);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
            }
        });

        ivEC_e.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llCAM.addView(ivEC);
                llCAM.removeView(ivEC_e);
                editor.putString("call_security","off");
                editor.commit();

                stopService(new Intent(getApplicationContext(),LockScr.class));

                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast,
                        (ViewGroup) findViewById(R.id.toast_layout_root));

                ImageView image = (ImageView) layout.findViewById(R.id.image);
                image.setImageResource(R.drawable.logo);
                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText("Call Security OFF");

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.BOTTOM, 0, 110);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
            }
        });

       /* if(preference.getString("EC","").equals("on")){
            llCAM.addView(ivEC_e);

        }
        else{
            llCAM.addView(ivEC);

        }



        ivEC_e.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llCAM.addView(ivEC);
                llCAM.removeView(ivEC_e);

                DPM=(DevicePolicyManager)getSystemService(Context.DEVICE_POLICY_SERVICE);
                NM=new ComponentName(getApplicationContext(),Admin.class);
                DPM.setCameraDisabled(NM,false);
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast,
                        (ViewGroup) findViewById(R.id.toast_layout_root));

                ImageView image = (ImageView) layout.findViewById(R.id.image);
                image.setImageResource(R.drawable.logo);
                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText("Camera Enabled !!!");

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.BOTTOM, 0, 110);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();

            }
        });*/

        //WD

        ivWD_e.setLayoutParams(params1);
        ivWD_e.setBackgroundResource(R.drawable.wipe1);
        ivWD.setLayoutParams(params1);
        ivWD.setBackgroundResource(R.drawable.wipe2);

        if(preference.getString("wipe","").equals("on")){
            llWIPE.addView(ivWD_e);

        }
        else{
            llWIPE.addView(ivWD);

        }

        ivWD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llWIPE.removeView(ivWD);
                llWIPE.addView(ivWD_e);

                editor.putString("wipe","on");
                editor.commit();

                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast,
                        (ViewGroup) findViewById(R.id.toast_layout_root));

                ImageView image = (ImageView) layout.findViewById(R.id.image);
                image.setImageResource(R.drawable.logo);
                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText("Wipe Data feature ON");

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.BOTTOM, 0, 110);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
            }
        });

        ivWD_e.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llWIPE.addView(ivWD);
                llWIPE.removeView(ivWD_e);

                editor.putString("wipe","off");
                editor.commit();
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast,
                        (ViewGroup) findViewById(R.id.toast_layout_root));

                ImageView image = (ImageView) layout.findViewById(R.id.image);
                image.setImageResource(R.drawable.logo);
                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText("Wipe Data feature OFF");

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.BOTTOM, 0, 110);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
            }
        });




    }
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();

    }

    @Override
    protected void onStop() {
        super.onStop();
        torch_floatingWindow.wm.addView(torch_floatingWindow.ll,torch_floatingWindow.parameters);
        if (Build.VERSION.SDK_INT >= 20) {
            //flashOff();
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {

            finish();

            return false;
        }
        return super.onKeyDown(keyCode, event);
    }


    public void flashOn() {

        Camera camera = Camera.open();
        Camera.Parameters parameters = camera.getParameters();
        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        camera.setParameters(parameters);
        camera.startPreview();

    }

    public void flashOn_l(){
        Camera mCam = Camera.open();
        Camera.Parameters p = mCam.getParameters();
        p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        mCam.setParameters(p);
        SurfaceTexture mPreviewTexture = new SurfaceTexture(0);
        try {
            mCam.setPreviewTexture(mPreviewTexture);
        } catch (IOException ex) {
            // Ignore
        }
        mCam.startPreview();
    }

    public void flashOff() {

        Camera camera2 = Camera.open();
        Camera.Parameters parameters2 = camera2.getParameters();
        parameters2.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        camera2.setParameters(parameters2);
        camera2.stopPreview();

    }
    public void turnOffFlashLight() {

        try {
            if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
                cam.stopPreview();
                cam.release();
                cam = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getBaseContext(), "Exception throws in turning off flashlight.", Toast.LENGTH_SHORT).show();
        }
    }
}
