package com.example.hungers.androiddefender;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class about_app extends AppCompatActivity {
    public static String PREFS_NAME="hungers";
    TextView t1,t2,t3,t4,t5,t6,t7,t8, t9,t10,t11,t12,t13,t14;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_app);
        t1=(TextView) findViewById(R.id.tv_tp);
        t2=(TextView) findViewById(R.id.tv_tpd);
        t3=(TextView) findViewById(R.id.tv_sp);
        t4=(TextView) findViewById(R.id.tv_spd);
        t5=(TextView) findViewById(R.id.tv_lp);
        t6=(TextView) findViewById(R.id.tv_lpd);
        t7=(TextView) findViewById(R.id.tv_cp);
        t8=(TextView) findViewById(R.id.tv_cpd);
        t9=(TextView) findViewById(R.id.tv_wp);
        t10=(TextView) findViewById(R.id.tv_wpd);
        t11=(TextView) findViewById(R.id.tv_dc);
        t12=(TextView) findViewById(R.id.tv_dcd);
        t13=(TextView) findViewById(R.id.tv_ec);
        t14=(TextView) findViewById(R.id.tv_ecd);

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/neo_bold.ttf");
        t1.setTypeface(type);
        t3.setTypeface(type);
        t5.setTypeface(type);
        t7.setTypeface(type);
        t9.setTypeface(type);
        t11.setTypeface(type);
        t13.setTypeface(type);


        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        t2.setTypeface(type1);
        t4.setTypeface(type1);
        t6.setTypeface(type1);
        t8.setTypeface(type1);
        t10.setTypeface(type1);
        t12.setTypeface(type1);
        t14.setTypeface(type1);


    }
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if (Build.VERSION.SDK_INT >= 22) {

            editor.putString("recent","on");
            editor.commit();
        }


    }
}
