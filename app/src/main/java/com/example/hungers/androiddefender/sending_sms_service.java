package com.example.hungers.androiddefender;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.telephony.SmsManager;
import android.widget.Toast;

public class sending_sms_service extends Service {
    public static String PREFS_NAME="hungers";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        final SharedPreferences preference = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preference.edit();

        //Toast.makeText(getApplicationContext(), "start service", Toast.LENGTH_SHORT).show();
        if (preference.getString("send", null).equals("yes")) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {


                    Intent act = new Intent(getApplicationContext(), SmsResultReceiver.class);
                    final PendingIntent sentPI = PendingIntent.getBroadcast(getApplicationContext(), 0, act, 0);
                    //Toast.makeText(getApplicationContext(), "send sms service", Toast.LENGTH_SHORT).show();
                    final SmsManager sms = SmsManager.getDefault();
                    sms.sendTextMessage(preference.getString("safe_number", null), null, "Sim Card Changed!!!", sentPI, null);
                    //Toast.makeText(getApplicationContext(), "end service", Toast.LENGTH_SHORT).show();
                    stopSelf();
                }
            }, 40000);
        }
    }
}
