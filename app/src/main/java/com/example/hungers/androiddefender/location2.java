package com.example.hungers.androiddefender;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.identity.intents.Address;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class location2 extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener , ResultCallback<Status> {

    //Define a request code to send to Google Play services
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "";
    private GoogleApiClient mGoogleApiClient;
    // private LocationRequest mLocationRequest;
    private double currentLatitude;
    private double currentLongitude;
    private  TextView mLatvalue;
    private TextView mLongvalue;

    // for getting Continuous update

    private final long LOC_UPDATE_INTERVAL = 10000; // 10s in milliseconds
    private final long LOC_FASTEST_UPDATE = 5000; // 5s in milliseconds
    protected LocationRequest mLocRequest;
    protected Location mCurLocation;

    //check location setting

    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    protected LocationSettingsRequest mLocationSettingsRequest;

    private boolean mHaveLocPerm = false;




    final Context context = this;
    private Button buttonGeofence;


    public static String PREFS_NAME="hungers";


  /* Sequence of code
  *Activity Lifecycle Methods : onCreate,onStart,onStop,onResume,onPause
  *GooglePlayServices Lifecycle methods : onConnected , onConnectionFailed, onConnectionSuspended,
    onLocationChanged
  *Other Function
  * Geofance code
   */


    /**
     * Activity Lifecycle methods
     */



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location2);



        mLatvalue = (TextView) findViewById(R.id.valLat);

        mLongvalue = (TextView) findViewById(R.id.valLong);

        mCurLocation = null;

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


        Button map = (Button) findViewById(R.id.map);

        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(preference.getString("gps","").equals("off")){
                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Please turn on GPS");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();
                    Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(500);

                }
                else if(preference.getString("gps","").equals("on")){
                    Intent i=new Intent(location2.this,MapsActivity.class);
                    startActivity(i);
                }


            }
        });

        // build the Play Services client object
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        //setting Location update setting request
        mLocRequest = new LocationRequest().setInterval(LOC_UPDATE_INTERVAL)
                .setFastestInterval(LOC_FASTEST_UPDATE)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        //check the status of location settings


        // checkLocationSettings();

        //  buildLocationSettingsRequest();


        //Add Geofence Dialogue

//        GeofenceController.getInstance().init(context);



    }








    @Override
    protected void onStart() {
        super.onStart();

        // Connect to Play Services
        GoogleApiAvailability gAPI = GoogleApiAvailability.getInstance();
        int resultCode = gAPI.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            gAPI.getErrorDialog(this, resultCode, 1).show();
        }
        else {
            mGoogleApiClient.connect();
            buildLocationSettingsRequest();
        }



    }
    @Override
    protected void onStop() {
        super.onStop();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


        if (mGoogleApiClient.isConnected() && preference.getString("location2","").equals("off")){
            mGoogleApiClient.disconnect();
        }
        else{
            editor.putString("location2","off");
            editor.commit();
        }

    }

    @Override
    protected void onResume() {

        super.onResume();

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


        if(preference.getString("location2","").equals("on")) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    editor.putString("location2","off");
                    editor.commit();
                    finish();
                }
            },1000);
        }

        if(preference.getString("req_for_loc_name","").equals("true")) {

            //Toast.makeText(context, "Name1", Toast.LENGTH_SHORT).show();
            try {
                updateUI();

            } catch (IOException e) {
                e.printStackTrace();
                String cityName = "NULL";
                String stateName = "NULL";
                String countryName ="NULL";
                String location_name = " "+cityName + " , "+ stateName+" , "+countryName+".";

                editor.putString("location_name",location_name);
                editor.putString("req_for_loc_name","false");
                editor.commit();
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //send_sms();
                    String sms1="TP "+preference.getString("latitude","")+" "+preference.getString("longitude","")+" "+
                            preference.getString("location_name","");
                    Intent act = new Intent(getApplicationContext(), SmsResultReceiver.class);
                    final PendingIntent sentPI = PendingIntent.getBroadcast(getApplicationContext(), 0, act, 0);
                    SmsManager sms = SmsManager.getDefault();
                    sms.sendTextMessage(preference.getString("out_number",""), null, sms1, sentPI, null);
                    finish();

                }
            }, 500);
        }
    }
//
//    @Override
//    protected void onPause() {
//
//
//    }


    //check location setting start

    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocRequest);
        mLocationSettingsRequest = builder.build();
        //  builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, mLocationSettingsRequest);



        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //  final LocationSettingsStates states= result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    location2.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.

                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made

                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to

                        break;
                    default:
                        break;
                }
                break;
        }
    }


    //check location setting end




    /**
     * Google Play Services Lifecycle methods
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        // If we're running on API 23 or above, we need to ask permission at runtime
        int permCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        }
        else {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocRequest, (LocationListener) this);
            initializeUI();
        }


    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.d(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    @Override
    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();
    }


    @Override
    public void onLocationChanged(Location location) {
        mCurLocation = location;

        try {
            updateUI();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    /**
     * Called when the user has been prompted at runtime to grant permissions
     */
    @Override
    public void onRequestPermissionsResult(int reqCode, String[] perms, int[] results){
        if (reqCode == 1) {
            if (results.length > 0 && results[0] == PackageManager.PERMISSION_GRANTED) {
                initializeUI();
            }
        }
    }

    /**
     * Update the location field values in the Activity with the given
     * values in the supplied Location object
     */
    public void setLocationFields(Location loc) {
        Log.d(TAG, "Updating location fields");
        if (loc != null) {
            mLatvalue.setText(String.format("%f", loc.getLatitude()));
            mLongvalue.setText(String.format("%f", loc.getLongitude()));


        }
    }

    /**
     * Retrieves the last known location. Assumes that permissions are granted.
     */


    protected void updateUI() throws IOException {
        // take the lat and long of the current location object and add it to the list

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if (mCurLocation != null) {

            String lat = String.valueOf(mCurLocation.getLatitude());
            String lng = String.valueOf(mCurLocation.getLongitude());

            mLatvalue.setText(lat);
            mLongvalue.setText(lng);

            editor.putString("latitude",lat);
            editor.putString("longitude",lng);
            editor.putString("gps","on");
            editor.commit();

        }

        WifiManager wifiManager = (WifiManager)this.context.getSystemService(Context.WIFI_SERVICE);


        if(isOnline() || wifiManager.isWifiEnabled()){

            if(preference.getString("req_for_loc_name","").equals("true")) {


                Double latti = Double.valueOf(preference.getString("latitude",""));
                Double longi = Double.valueOf(preference.getString("longitude",""));

                Geocoder geocoder = new Geocoder(this, Locale.getDefault());

                List<android.location.Address> add = geocoder.getFromLocation(latti,longi,1);

                String cityName = add.get(0).getAddressLine(0);
                String stateName = add.get(0).getAddressLine(1);
                String countryName = add.get(0).getAddressLine(2);

                String location_name = " "+cityName + " , "+ stateName+" , "+countryName+".";

                editor.putString("location_name",location_name);
                editor.putString("req_for_loc_name","false");
                editor.commit();

                Toast.makeText(context, preference.getString("location_name",""), Toast.LENGTH_SHORT).show();

            }
        }
        else{

        }


    }

    protected void initializeUI() {
        // start by getting the last known location as a starting point
        if (mCurLocation == null) {
            try{
                mCurLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            }
            catch (SecurityException e)
            {
                Log.i(TAG,"Permission Denied while initizlizeUI");
            }

            // clear the locations list
            //mLocationList.setText("");

            try {
                updateUI();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }



    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor=preference.edit();

        if (mGoogleApiClient.isConnected() && preference.getString("location2","").equals("off")){
            mGoogleApiClient.disconnect();
        }
        else{
            editor.putString("location2","off");
            editor.commit();
        }


    }


    @Override
    public void onResult(Status status) {

    }


    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}