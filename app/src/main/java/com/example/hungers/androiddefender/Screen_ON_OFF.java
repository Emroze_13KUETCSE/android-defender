package com.example.hungers.androiddefender;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;

import static android.content.Context.MODE_PRIVATE;

public class Screen_ON_OFF extends BroadcastReceiver {
    public static String PREFS_NAME = "hungers";

    public Screen_ON_OFF() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        final SharedPreferences preference = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor = preference.edit();


        if(action.equals(Intent.ACTION_SCREEN_OFF) && preference.getString("lock_scren","").equals("on"))
        {
            /*Vibrator vib = (Vibrator) context.getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
            vib.vibrate(50);*/
            if(preference.getString("count","").equals("1")){

                int i;
                i=Integer.parseInt(preference.getString("count",""));
                i++;

                editor.putString("count",String.valueOf(i));
                editor.commit();

                if (android.os.Build.VERSION.SDK_INT >= 23) {

                    if (Settings.canDrawOverlays(context.getApplicationContext())) {
                        context.startService(new Intent(context,LockScr.class));
                        editor.putString("LockScr","1");
                        editor.commit();
                    }
                }else{
                    context.startService(new Intent(context,LockScr.class));
                    editor.putString("LockScr","1");
                    editor.commit();

                }

                // context.startService(new Intent(context,photo_lock.class));

            }
        }

    }
}
