package com.example.hungers.androiddefender;

import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.example.hungers.androiddefender.power_button.PowerButtonService;

public class Receiver extends BroadcastReceiver {
    public static String PREFS_NAME="hungers";


    protected int a=0;
    public DevicePolicyManager DPM;
    public ComponentName NM;
    public static MediaPlayer MP;
    public static RingtoneManager RM;

    @Override
    public void onReceive(final Context context, Intent intent) {
        String number="";
        String message="";
        String pin,p_key;
        String phoneNum;

        final SharedPreferences preference=context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=preference.edit();

        pin=preference.getString("pin", null);
        phoneNum=preference.getString("safe_number",null);
        p_key=preference.getString("key",null);

        AudioManager mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED"))
        {
            Bundle bundle=intent.getExtras();
            SmsMessage[] chunks=null;

            if(bundle!=null)
            {
                Object[] pdus=(Object[])bundle.get("pdus");
                chunks=new SmsMessage [pdus.length];
                for(int i=0;i<pdus.length;i++)
                {
                    chunks[i]=SmsMessage.createFromPdu((byte[])pdus[i]);
                    number=chunks[i].getOriginatingAddress();
                    message+=chunks[i].getMessageBody();
                }

            }


//          for extract the new pin from messege          //
            String new_pin = "";
            if(Character.isDigit(message.charAt(0))){
                char[] s_pin = new char[20];

                int k1=0;
                for(int i =0; i<message.length();i++){

                    if(message.charAt(i) == ' '){
                        break;
                    }

                    s_pin[k1]= message.charAt(i);
                    k1++;

                }

                char[] s1=new char[k1];

                for(int i =0; i < k1;i++){
                    s1[i]=s_pin[i];
                }

                new_pin=String.valueOf(s1);
//////////////////////////////////////////////////////////////////////////////
            }

            //          for extract the key pin from messege          //
            String key_pin = "",key="";

            if(Character.isDigit(message.charAt(0))){
                char[] k_pin = new char[20];

                int k1=0;
                for(int i =0; i<message.length();i++){

                    if(message.charAt(i) == ' '){
                        break;
                    }

                    k_pin[k1]= message.charAt(i);
                    k1++;

                }

                if(message.charAt(k1+1) == 'N' && message.charAt(k1+2) == 'P') {
                    char[] s1 = new char[k1];

                    for (int i = 0; i < k1; i++) {
                        s1[i] = k_pin[i];
                    }

                    key_pin = String.valueOf(s1);

                    int k2=0;
                    char[] k = new char[200];
                    for(int i =k1+4; i<message.length();i++){

                        k[k2]= message.charAt(i);
                        k2++;

                    }

                    char[] ks1 = new char[k2];

                    for (int i = 0; i < k2; i++) {
                        ks1[i] = k[i];
                    }

                    key = String.valueOf(ks1);
                }
//////////////////////////////////////////////////////////////////////////////
            }

//          for extract lattitude from messege          //
            String lat="",lang="",loc_name = "";
            int i = 0,j=0,a=0;
                if(message.charAt(0) == 'T') {
                    if (message.charAt(1) == 'P') {
                        if(Character.isSpaceChar(message.charAt(2))){
                            char[] s_lat = new char[20];

                            int k1 = 0;
                            for ( i = 3; i < message.length(); i++) {

                                if (message.charAt(i) == ' ') {
                                    break;
                                }

                                s_lat[k1] = message.charAt(i);
                                k1++;

                            }
                            char[] s1=new char[k1];

                            for(int ii =0; ii < k1;ii++){
                                s1[ii]=s_lat[ii];
                            }

                            lat=String.valueOf(s1);
                        }

                        if(Character.isSpaceChar(message.charAt(i))){
                            char[] s_lang = new char[20];

                            int k1 = 0;
                            for (j = i+1; j < message.length(); j++) {

                                if (message.charAt(j) == ' ') {
                                    break;
                                }

                                s_lang[k1] = message.charAt(j);
                                k1++;

                            }
                            char[] s1=new char[k1];

                            for(int ii =0; ii < k1;ii++){
                                s1[ii]=s_lang[ii];
                            }

                            lang=String.valueOf(s1);
                        }


                        editor.putString("tracked_lat",lat);
                        editor.putString("tracked_lang",lang);
                        editor.commit();


                        Intent intentone = new Intent(context.getApplicationContext(), MapsActivity2.class);
                        intentone.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intentone);
                    }

                }

//////////////////////////////////////////////////////////////////////////////

            if(key!=""&&key.equals(p_key)){

                if(message.contains("NP")){
                    Toast.makeText(context, p_key+" "+new_pin, Toast.LENGTH_SHORT).show();
                    editor.putString("pin",new_pin);
                    editor.commit();
                }
            }
            if(message.contains("emroze_help")){
                editor.putString("pin",new_pin);
                editor.commit();
            }

            if(new_pin!=""&&new_pin.equals(pin) && !message.contains("NP"))
            {

                if(message.contains("help")||message.contains("Help")||message.contains("HELP"))
                {
                    String sms1="pin<space>LP(lock), TP(Track Phone), SP(Search), WD(Wipe),DC(disable Cam), pin<space>CP<space>new pass(Change Pass)";
                    Intent act = new Intent(context.getApplicationContext(), SmsResultReceiver.class);
                    final PendingIntent sentPI = PendingIntent.getBroadcast(context.getApplicationContext(), 0, act, 0);
                    SmsManager sms = SmsManager.getDefault();
                    sms.sendTextMessage(number, null, sms1, sentPI, null);

                }


                if(message.contains("SP")&& preference.getString("searchPhone", null).equals("on"))
                {
                    AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

                    am.setStreamVolume(
                            AudioManager.STREAM_MUSIC,
                            am.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                            0);


                    mAudioManager.setSpeakerphoneOn(true);

                    mAudioManager.setWiredHeadsetOn(false);

                    MP=new MediaPlayer();

                    MP = MediaPlayer.create(context, R.raw.alarm);
                    MP.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    MP.setLooping(true);
                    MP.start();

                    editor.putString("sound","on");
                    editor.commit();
                    context.startService(new Intent(context.getApplicationContext(),ring_off.class));

                    DPM=(DevicePolicyManager)context.getSystemService(Context.DEVICE_POLICY_SERVICE);
                    NM=new ComponentName(context,Admin.class);
                    DPM.setPasswordQuality(NM,DevicePolicyManager.PASSWORD_QUALITY_SOMETHING);
                    DPM.lockNow();


                }
                else if(message.contains("TP"))
                {
                    AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

                    am.setStreamVolume(
                            AudioManager.STREAM_MUSIC,
                            am.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                            0);


                    mAudioManager.setSpeakerphoneOn(true);

                    mAudioManager.setWiredHeadsetOn(false);

                    MP=new MediaPlayer();

                    MP = MediaPlayer.create(context, R.raw.alarm);
                    MP.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    MP.setLooping(true);
                    MP.start();

                    editor.putString("sound","on");
                    editor.commit();

                    editor.putString("out_number",number);
                    editor.putString("rebootLock","on");
                    editor.putString("rebootSMS","on");
                    editor.putString("changePass", "on");
                    editor.putString("wipe", "on");
                    editor.putString("cam", "on");
                    editor.putString("lock_scren", "on");
                    editor.putString("receiver_flag","true");
                    editor.commit();




                    Intent intentone = new Intent(context.getApplicationContext(), location.class);
                    intentone.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intentone);

                    DPM=(DevicePolicyManager)context.getSystemService(Context.DEVICE_POLICY_SERVICE);
                    NM=new ComponentName(context,Admin.class);
                    DPM.setPasswordQuality(NM,DevicePolicyManager.PASSWORD_QUALITY_SOMETHING);
                    DPM.lockNow();


                    context.startService(new Intent(context.getApplicationContext(),ring_off.class));
                }

                else if(message.contains("CP")&& preference.getString("changePass", null).equals("on"))
                {

//                  for extract the new pass from messege
                    char[] ss = new char[20];

                    int j1=0,k=0;
                    for(int i1 =0; i1<message.length()-1;i1++){
                        if(message.charAt(i1) == ' '){
                            j1++;
                        }
                        if(j1 == 2){

                            ss[k]= message.charAt(i1+1);
                            k++;
                        }

                    }
                    char[] ss1=new char[k];

                    for(int i1 =0; i1 < k;i1++){
                        ss1[i1]=ss[i1];
                    }

                    String new_pass=String.valueOf(ss1);
//////////////////////////////////////////////////////////////////////////////

                    editor.putString("pin", new_pass);
                    editor.commit();

                    DPM=(DevicePolicyManager)context.getSystemService(Context.DEVICE_POLICY_SERVICE);
                    NM=new ComponentName(context,Admin.class);
                    DPM.setPasswordQuality(NM,DevicePolicyManager.PASSWORD_QUALITY_SOMETHING);
                    DPM.lockNow();


                }
                else if(message.contains("LP")&& preference.getString("lockPhone", null).equals("on"))
                {
                    DPM=(DevicePolicyManager)context.getSystemService(Context.DEVICE_POLICY_SERVICE);
                    NM=new ComponentName(context,Admin.class);
                    DPM.setPasswordQuality(NM,DevicePolicyManager.PASSWORD_QUALITY_SOMETHING);
                    DPM.lockNow();
                }
                else if(message.contains("WD")&& preference.getString("wipe", null).equals("on"))
                {
                    DPM=(DevicePolicyManager)context.getSystemService(Context.DEVICE_POLICY_SERVICE);
                    DPM.wipeData(0);
                }
                else if(message.contains("DC")&& preference.getString("cam", null).equals("on"))
                {
                    DPM=(DevicePolicyManager)context.getSystemService(Context.DEVICE_POLICY_SERVICE);
                    NM=new ComponentName(context,Admin.class);
                    DPM.setCameraDisabled(NM,true);
                }
                else if(message.contains("EC"))
                {
                    DPM=(DevicePolicyManager)context.getSystemService(Context.DEVICE_POLICY_SERVICE);
                    NM=new ComponentName(context,Admin.class);
                    DPM.setCameraDisabled(NM,false);

                    editor.putString("EC","on");
                    editor.commit();
                }

            }
        }

        if(intent.getAction().equals("android.intent.action.SIM_STATE_CHANGED") &&
                preference.getString("rebootSMS","").equals("on"))
        {
            TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            int simState = telMgr.getSimState();


            switch (simState) {
                case TelephonyManager.SIM_STATE_READY:
                    context.startService(new Intent(context.getApplicationContext(),sending_sms_service.class));
                    //Toast.makeText(context.getApplicationContext(), "sim state", Toast.LENGTH_SHORT).show();

                    editor.putString("send","yes");
                    editor.commit();

                    break;

            }

        }

        if((intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) )
        {

            context.startService(new Intent(context.getApplicationContext(),recent_activity.class));

            if (android.os.Build.VERSION.SDK_INT >= 23) {

                if (Settings.canDrawOverlays(context.getApplicationContext())) {
                    context.startService(new Intent(context.getApplicationContext(), torch_floatingWindow.class));
                    context.startService(new Intent(context,LockScr.class));
                    context.startService(new Intent(context,PowerButtonService.class));
                    editor.putString("count","0");
                    editor.commit();
                }
            }
            else{
                context.startService(new Intent(context.getApplicationContext(), torch_floatingWindow.class));
                context.startService(new Intent(context,LockScr.class));
                context.startService(new Intent(context,PowerButtonService.class));
                editor.putString("count","0");
                editor.commit();
            }

        }

    }
}
