package com.example.hungers.androiddefender;

import android.util.Log;

import java.security.SecureRandom;

import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by emroz on 1/8/2017.
 */
public class Helper {

    public Helper(){}

    public SecretKeySpec sksGenerator(){
        SecretKeySpec sks = null;
        try {
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            sr.setSeed("Rhenium".getBytes());
            KeyGenerator kg = KeyGenerator.getInstance("AES");
            kg.init(128,sr);
            sks = new SecretKeySpec((kg.generateKey()).getEncoded(), "AES");
        } catch (Exception e) {
            Log.e("TAG", "AES secret key spec error");
        }

        return sks;
    }

    public byte[] encodeData(int x, SecretKeySpec sks){
        byte[] encodedBytes = null;
        try {
            javax.crypto.Cipher c = javax.crypto.Cipher.getInstance("AES");
            c.init(javax.crypto.Cipher.ENCRYPT_MODE, sks);
            encodedBytes = c.doFinal(Integer.toString(x).getBytes());
        } catch (Exception e) {
            Log.e("TAG", "AES encryption error");
        }

        return encodedBytes;
    }

    public int decodeData(byte[] encodedBytes, SecretKeySpec sks){
        byte[] decodedBytes = null;
        try {
            javax.crypto.Cipher c = javax.crypto.Cipher.getInstance("AES");
            c.init(javax.crypto.Cipher.DECRYPT_MODE, sks);
            decodedBytes = c.doFinal(encodedBytes);
        } catch (Exception e) {
            Log.e("TAG", "AES decryption error");
        }

        return Integer.parseInt(new String(decodedBytes));
    }
}

