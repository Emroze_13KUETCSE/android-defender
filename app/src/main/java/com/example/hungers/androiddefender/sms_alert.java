package com.example.hungers.androiddefender;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Vibrator;

import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class sms_alert extends Activity {
    public static String PREFS_NAME="hungers";
    TextView tv,tv1,tv2;
    FrameLayout ll;
    Button b1,b2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams windowManager=getWindow().getAttributes();
        windowManager.dimAmount=0.60f;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.activity_sms_alert);


        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        stopService(new Intent(getApplicationContext(),torch_floatingWindow.class));


        tv=(TextView) findViewById(R.id.tv1);
        tv1=(TextView) findViewById(R.id.tv_rs);
        tv2=(TextView) findViewById(R.id.tv_rp);

        b1=(Button) findViewById(R.id.btn_later);
        b2=(Button) findViewById(R.id.btn_star);

        ll =(FrameLayout) findViewById(R.id.fl1);

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/neo_bold.ttf");
        tv.setTypeface(type);

        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        b1.setTypeface(type1);
        b2.setTypeface(type1);
        tv1.setTypeface(type1);
        tv2.setTypeface(type1);

        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        b1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        b1.getBackground().setAlpha(190);
                        break;
                    case MotionEvent.ACTION_UP:
                        b1.getBackground().setAlpha(255);
                        Intent intent4 = new Intent();
                        intent4.setComponent(new ComponentName("com.android.settings","com.android.settings.Settings"));
                        startActivity(intent4);

                        break;
                }
                return false;
            }
        });
        b2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        b2.getBackground().setAlpha(190);
                        break;
                    case MotionEvent.ACTION_UP:
                        b2.getBackground().setAlpha(255);



                        send_sms();

                        finish();

                        break;
                }
                return false;
            }
        });
    }

    private void send_sms() {
        SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor=preference.edit();

        if(preference.getString("forgot", "").equals("true")){
            editor.putString("forgot","false");
            editor.commit();

            String msg = preference.getString("pinInputed", null);
            msg = msg.concat(" "+"NP"+" "+preference.getString("keyInputed", ""));
            sendSMS(preference.getString("numInputed", null),msg);

        }else {
            if(preference.getString("seq", "").equals("1"))
            {
                String msg = preference.getString("pinInputed", "");
                msg = msg.concat(" "+"TP");
                sendSMS(preference.getString("numInputed", ""),msg);

            }else if(preference.getString("seq", "").equals("2")) {
                String msg = preference.getString("pinInputed", "");
                msg = msg.concat(" "+"SP");
                sendSMS(preference.getString("numInputed", ""),msg);
            }
            else if(preference.getString("seq", "").equals("3")) {
                String msg = preference.getString("pinInputed", "");
                msg = msg.concat(" "+"LP");
                sendSMS(preference.getString("numInputed", ""),msg);
            }
            else if(preference.getString("seq", "").equals("4")) {
                String msg = preference.getString("pinInputed", "");
                msg = msg.concat(" "+"CP"+" "+preference.getString("passInputed", ""));
                sendSMS(preference.getString("numInputed", ""),msg);
            }
            else if(preference.getString("seq", "").equals("5")) {
                String msg = preference.getString("pinInputed", "");
                msg = msg.concat(" "+"WD");
                sendSMS(preference.getString("numInputed", ""),msg);
            }
            else if(preference.getString("seq", "").equals("6")) {
                String msg = preference.getString("pinInputed", "");
                msg = msg.concat(" "+"DC");
                sendSMS(preference.getString("numInputed", ""),msg);
            }

        }

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,
                (ViewGroup) findViewById(R.id.toast_layout_root));

        ImageView image = (ImageView) layout.findViewById(R.id.image);
        image.setImageResource(R.drawable.logo);
        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText("SMS is sending");

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.BOTTOM, 0, 110);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();

        Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        vib.vibrate(500);
    }

    // for sms delivery report
    BroadcastReceiver sendBroadcastReceiver = new sms_alert.SentReceiver();
    BroadcastReceiver deliveryBroadcastReciever = new sms_alert.DeliverReceiver();

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliveryBroadcastReciever);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliveryBroadcastReciever);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void sendSMS(String phoneNumber, String message) {
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

        registerReceiver(deliveryBroadcastReciever, new IntentFilter(DELIVERED));
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);


    }

    class DeliverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "SMS delivered",
                            Toast.LENGTH_SHORT).show();
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(getBaseContext(), "sms not delivered",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }

    class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {

            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast,
                    (ViewGroup) findViewById(R.id.toast_layout_root));

            ImageView image = (ImageView) layout.findViewById(R.id.image);
            image.setImageResource(R.drawable.logo);
            TextView text = (TextView) layout.findViewById(R.id.text);
            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.BOTTOM, 0, 110);
            toast.setView(layout);

            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    text.setText("SMS sent successfully");
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.show();

                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:

                    text.setText("Generic failure.Please send again");
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.show();

                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    text.setText("No service.Please send again");
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.show();

                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    text.setText("No service.Please send again");
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.show();

                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    text.setText("Radio off.Please send again");
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.show();

                    break;
            }

        }
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if (Build.VERSION.SDK_INT >= 22) {

            editor.putString("recent","on");
            editor.commit();
        }


    }
}
