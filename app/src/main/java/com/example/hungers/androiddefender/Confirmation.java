package com.example.hungers.androiddefender;

import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import javax.crypto.spec.SecretKeySpec;


public class Confirmation extends AppCompatActivity {

    protected ImageView iv;
    protected RelativeLayout.LayoutParams params;
    protected RelativeLayout rl;
    protected Button confirm;
    protected ImageView pic_lock;
    protected int count;
    protected final String[] preKeyNameX = {"pKey1X","pKey2X","pKey3X"},preKeyNameY = {"pKey1Y","pKey2Y","pKey3Y"};
    protected final String[] keyNameX = {"Key1X","Key2X","Key3X"},keyNameY = {"Key1Y","Key2Y","Key3Y"};
    protected final String fileName = "Security_Key";
    protected boolean exactMatch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);

        count = 0;
        rl = (RelativeLayout) findViewById(R.id.relative_confirmation);

        final SharedPreferences security = getSharedPreferences(fileName , MODE_PRIVATE);

        byte[] decode = Base64.decode(security.getString("sks","0"),Base64.DEFAULT);
        final SecretKeySpec secks = new SecretKeySpec(decode,0,decode.length,"AES");
        final Checker checker = new Checker(secks,getApplicationContext());

        confirm = (Button) findViewById(R.id.confirm);
        confirm.setEnabled(false);
        confirm.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        exactMatch = true;

        pic_lock = (ImageView) findViewById(R.id.picture_lock_confirmation);
        pic_lock.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int x = (int) motionEvent.getX();
                int y = (int) motionEvent.getY();

                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if(count < 3){
                            if(!checker.checkX(x,count) || !checker.checkY(y,count)){
                                exactMatch = false;
                                drawRedCircle(x,y,rl);
                            }
                            else
                                drawCircle(x,y,rl);
                            count++;

                            if(count >= 1){
                                confirm.setEnabled(true);
                                confirm.setOnTouchListener(new View.OnTouchListener() {
                                    @Override
                                    public boolean onTouch(View view, MotionEvent motionEvent) {
                                        if(count == 3 && exactMatch){
                                            SharedPreferences.Editor editor = security.edit();
                                            for(int i=0;i<3;i++){
                                                editor.putString(keyNameX[i],security.getString(preKeyNameX[i],""));
                                                editor.commit();
                                                editor.putString(keyNameY[i],security.getString(preKeyNameY[i],""));
                                                editor.commit();
                                            }

                                            finish();
                                        }
                                        else{
                                            startActivity(new Intent(getApplicationContext(),Retry.class));
                                            finish();
                                        }
                                        return true;
                                    }
                                });

                                if(count == 3){
                                    pic_lock.setOnTouchListener(new View.OnTouchListener() {
                                        @Override
                                        public boolean onTouch(View view, MotionEvent motionEvent) {
                                            return false;
                                        }
                                    });
                                }
                            }
                        }
                        break;
                }
                return true;
            }
        });



        Button cancel = (Button) findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Main.class));
            }
        });

    }

    protected void drawRedCircle(int x, int y,RelativeLayout rl){
        iv = new ImageView(this);
        iv.setImageResource(R.drawable.error_circle);
        params = new RelativeLayout.LayoutParams(100,100);
        params.setMargins(x-50,y-50,0,0);
        rl.addView(iv, params);
    }

    protected void drawCircle(int x, int y,RelativeLayout rl){
        iv = new ImageView(this);
        iv.setImageResource(R.drawable.circle5);
        params = new RelativeLayout.LayoutParams(100,100);
        params.setMargins(x-50,y-50,0,0);
        rl.addView(iv, params);
    }

}
