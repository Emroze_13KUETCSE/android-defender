package com.example.hungers.androiddefender;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.OnBoomListenerAdapter;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

public class FloatingWindow extends AppCompatActivity {
    public static String PREFS_NAME = "hungers";
    SwitchCompat s1, s2, s3, s4, s5, s6, s7, s8;
    LinearLayout l1, l2, l3, l4, l5, l6, l7, l8;
    private BoomMenuButton bmb0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        setContentView(R.layout.activity_floating_window);
        initUI();


        l1 = (LinearLayout) findViewById(R.id.ll1);
        l2 = (LinearLayout) findViewById(R.id.ll2);
        l3 = (LinearLayout) findViewById(R.id.ll3);
        l4 = (LinearLayout) findViewById(R.id.ll4);
        l5 = (LinearLayout) findViewById(R.id.ll5);
        l6 = (LinearLayout) findViewById(R.id.ll6);
        l7 = (LinearLayout) findViewById(R.id.ll7);
        l8 = (LinearLayout) findViewById(R.id.ll8);


        s1 = (SwitchCompat) findViewById(R.id.compatSwitch1);
        s2 = (SwitchCompat) findViewById(R.id.compatSwitch2);
        s3 = (SwitchCompat) findViewById(R.id.compatSwitch3);
        s4 = (SwitchCompat) findViewById(R.id.compatSwitch4);
        s5 = (SwitchCompat) findViewById(R.id.compatSwitch5);
        s6 = (SwitchCompat) findViewById(R.id.compatSwitch6);
        s7 = (SwitchCompat) findViewById(R.id.s1);
        s8 = (SwitchCompat) findViewById(R.id.s2);


        final SharedPreferences preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor = preference.edit();


//      reboot sms
        if (preference.getString("rebootSMS", null) == null) {
            editor.putString("rebootSMS", "on");
            editor.commit();
        }

        if (preference.getString("rebootSMS", null).equals("off")) {
            s7.setChecked(false);
        } else {
            s7.setChecked(true);
        }

        if (s7.isChecked()) {
            editor.putString("rebootSMS", "on");
            editor.commit();
        } else {
            editor.putString("rebootSMS", "off");
            editor.commit();
        }

        s7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (!isChecked) {

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("After Reboot SMS OFF");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("rebootSMS", "off");
                    editor.commit();
                } else {

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("After Reboot SMS ON");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("rebootSMS", "on");
                    editor.commit();
                }

            }
        });

//////////////////////////////////////////////////////////////////////////////////

        //      reboot lock
        if(preference.getString("lock_scren",null)==null){
            editor.putString("lock_scren", "on");
            editor.commit();
        }

        if(preference.getString("lock_scren",null).equals("off")){
            s8.setChecked(false);
        } else{
            s8.setChecked(true);
        }

        if(s8.isChecked()){
            editor.putString("lock_scren", "on");
            editor.commit();
        }
        else{
            editor.putString("lock_scren", "off");
            editor.commit();
        }

        s8.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if(!isChecked){

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("After Reboot auto Lock OFF");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("lock_scren", "off");
                    editor.commit();
                }
                else{

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("After Reboot auto Lock ON");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("lock_scren", "on");
                    editor.commit();
                }

            }
        });

//////////////////////////////////////////////////////////////////////////////////

//      serach phone
        if (preference.getString("searchPhone", null) == null) {
            editor.putString("searchPhone", "on");
            editor.commit();
        }

        if (preference.getString("searchPhone", null).equals("off")) {
            s1.setChecked(false);
        } else {
            s1.setChecked(true);
        }

        if (s1.isChecked()) {
            editor.putString("searchPhone", "on");
            editor.commit();
        } else {
            editor.putString("searchPhone", "off");
            editor.commit();
        }

        s1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (!isChecked) {

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Search Phone OFF");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("searchPhone", "off");
                    editor.commit();
                } else {

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Search Phone ON");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("searchPhone", "on");
                    editor.commit();
                }

            }
        });

////////////////////////////////////////////////////////////////////////////////////


        //      serach phone
        if (preference.getString("lockPhone", null) == null) {
            editor.putString("lockPhone", "on");
            editor.commit();
        }

        if (preference.getString("lockPhone", null).equals("off")) {
            s2.setChecked(false);
        } else {
            s2.setChecked(true);
        }

        if (s2.isChecked()) {
            editor.putString("lockPhone", "on");
            editor.commit();
        } else {
            editor.putString("lockPhone", "off");
            editor.commit();
        }

        s2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (!isChecked) {

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Lock Phone OFF");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("lockPhone", "off");
                    editor.commit();
                } else {

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Lock Phone ON");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("lockPhone", "on");
                    editor.commit();
                }

            }
        });

//////////////////////////////////////////////////////////////////////////////////


        //      serach phone
        if (preference.getString("changePass", null) == null) {
            editor.putString("changePass", "on");
            editor.commit();
        }

        if (preference.getString("changePass", null).equals("off")) {
            s3.setChecked(false);
        } else {
            s3.setChecked(true);
        }

        if (s3.isChecked()) {
            editor.putString("changePass", "on");
            editor.commit();
        } else {
            editor.putString("changePass", "off");
            editor.commit();
        }

        s3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (!isChecked) {

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Change Password OFF");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("changePass", "off");
                    editor.commit();
                } else {

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Change Password ON");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("changePass", "on");
                    editor.commit();
                }

            }
        });

//////////////////////////////////////////////////////////////////////////////////


        //      serach phone
        if (preference.getString("call", null) == null) {
            editor.putString("call", "on");
            editor.commit();
        }

        if (preference.getString("call", null).equals("off")) {
            s4.setChecked(false);
        } else {
            s4.setChecked(true);
        }

        if (s4.isChecked()) {
            editor.putString("call", "on");
            editor.commit();
        } else {
            editor.putString("call", "off");
            editor.commit();
        }

        s4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (!isChecked) {

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Call Security OFF");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("call", "off");
                    editor.commit();
                } else {

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Call Security ON");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("call", "on");
                    editor.commit();
                }

            }
        });

//////////////////////////////////////////////////////////////////////////////////


        //serach phone
        if (preference.getString("wipe", null) == null) {
            editor.putString("wipe", "on");
            editor.commit();
        }

        if (preference.getString("wipe", null).equals("off")) {
            s5.setChecked(false);
        } else {
            s5.setChecked(true);
        }

        if (s5.isChecked()) {
            editor.putString("wipe", "on");
            editor.commit();
        } else {
            editor.putString("wipe", "off");
            editor.commit();
        }

        s5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (!isChecked) {

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Wipe Data OFF");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("wipe", "off");
                    editor.commit();
                } else {

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Wipe Data ON");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("wipe", "on");
                    editor.commit();
                }

            }
        });

//////////////////////////////////////////////////////////////////////////////////


        //serach phone
        if (preference.getString("cam", null) == null) {
            editor.putString("cam", "on");
            editor.commit();
        }

        if (preference.getString("cam", null).equals("off")) {
            s6.setChecked(false);
        } else {
            s6.setChecked(true);
        }

        if (s6.isChecked()) {
            editor.putString("cam", "on");
            editor.commit();
        } else {
            editor.putString("cam", "off");
            editor.commit();
        }

        s6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (!isChecked) {

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Disable Camera OFF");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("cam", "off");
                    editor.commit();
                } else {

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.logo);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Disable Camera ON");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 110);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    editor.putString("cam", "on");
                    editor.commit();
                }

            }
        });

//////////////////////////////////////////////////////////////////////////////////

    }

    private void initUI() {

        final SharedPreferences preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor = preference.edit();
        bmb0 = (BoomMenuButton) findViewById(R.id.bmb);
        assert bmb0 != null;
        bmb0.setButtonEnum(ButtonEnum.Ham);
        bmb0.setPiecePlaceEnum(PiecePlaceEnum.HAM_4);
        bmb0.setButtonPlaceEnum(ButtonPlaceEnum.HAM_4);
        for (int i = 0; i < bmb0.getPiecePlaceEnum().pieceNumber(); i++) {
            bmb0.addBuilder(BuilderManager.getHamButtonBuilder2());

        }

        bmb0.setOnBoomListener(new OnBoomListenerAdapter() {
            @Override
            public void onBoomWillShow() {
                super.onBoomWillShow();
                // logic here
            }
        });


        bmb0.setOnBoomListener(new OnBoomListener() {
            @Override
            public void onClicked(int index, BoomButton boomButton) {

                if (index == 0) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            DevicePolicyManager DPM;
                            ComponentName NM;

                            DPM = (DevicePolicyManager) getApplication().getSystemService(Context.DEVICE_POLICY_SERVICE);
                            NM = new ComponentName(getApplication(), Admin.class);
                            DPM.setPasswordQuality(NM, DevicePolicyManager.PASSWORD_QUALITY_SOMETHING);
                            DPM.lockNow();

                            finish();

                        }
                    }, 950);

                } else if (index == 1) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Intent i = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(i);
                        }
                    }, 950);

                } else if (index == 2) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(FloatingWindow.this, MainActivity.class);
                            startActivity(i);
                        }
                    }, 950);

                } else if (index == 3) {
                    editor.putString("f", "off");
                    editor.commit();
                    finish();
                }


            }

            @Override
            public void onBackgroundClick() {

            }

            @Override
            public void onBoomWillHide() {

            }

            @Override
            public void onBoomDidHide() {
                l1.setVisibility(View.VISIBLE);
                l2.setVisibility(View.VISIBLE);
                l3.setVisibility(View.VISIBLE);
                l4.setVisibility(View.VISIBLE);
                l5.setVisibility(View.VISIBLE);
                l6.setVisibility(View.VISIBLE);
                l7.setVisibility(View.VISIBLE);
                l8.setVisibility(View.VISIBLE);

            }

            @Override
            public void onBoomWillShow() {
                l1.setVisibility(View.INVISIBLE);
                l2.setVisibility(View.INVISIBLE);
                l3.setVisibility(View.INVISIBLE);
                l4.setVisibility(View.INVISIBLE);
                l5.setVisibility(View.INVISIBLE);
                l6.setVisibility(View.INVISIBLE);
                l7.setVisibility(View.INVISIBLE);
                l8.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onBoomDidShow() {

            }
        });

    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        final SharedPreferences preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor = preference.edit();

        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
            return false;

        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        stopService(new Intent(getApplicationContext(),torch_floatingWindow.class));

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if (Build.VERSION.SDK_INT >= 22) {

            editor.putString("recent","off");
            editor.commit();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        final SharedPreferences preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor = preference.edit();
        if(preference.getString("f","").equals("off")){
            stopService(new Intent(getApplicationContext(),torch_floatingWindow.class));
            editor.putString("f","on");
            editor.commit();
        }else if(preference.getString("f","").equals("on")){
            startService(new Intent(getApplicationContext(),torch_floatingWindow.class));
        }

        finish();
    }
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if (Build.VERSION.SDK_INT >= 22) {

            editor.putString("recent","on");
            editor.commit();
        }


    }
}
