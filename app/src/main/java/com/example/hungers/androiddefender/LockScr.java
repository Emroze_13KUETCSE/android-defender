package com.example.hungers.androiddefender;

import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.app.WallpaperManager;
import android.app.admin.DevicePolicyManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.renderscript.Type;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;
import java.util.SortedMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

public class LockScr extends Service {
    public static WindowManager wm;
    public static LinearLayout ll2;
    private LinearLayout ll21;
    private LinearLayout l1;
    private LinearLayout l2;
    private LinearLayout l3;
    private LinearLayout l4;
    TextClock tc;

    public static TextView tv;

    private String pin = "";

    public static String PREFS_NAME="hungers";
    private android.widget.Button ok,cancel;
    Button b1,b2,b3,b4,b5,b6,b7,b8,b9,b0;
    //EditText et1;
    ImageView iv,iv_loc;

    private android.widget.Button lock;

    BroadcastReceiver receiver;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        //startService(new Intent(getApplicationContext(),ServiceLock.class));
        /*Intent i = new Intent(getApplicationContext(),behind_scr.class);
        startActivity(i);*/
        //

        startForeground();


        /*final Handler handler1 = new Handler();
        final Timer timer1 = new Timer();

        final TimerTask timerTask1 = new TimerTask() {
            @Override
            public void run() {
                handler1.post(new Runnable() {
                    @Override
                    public void run() {

                        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
                        filter.addAction(Intent.ACTION_SCREEN_OFF);

                        receiver = new Screen_ON_OFF();
                        registerReceiver(receiver, filter);


                    }
                });
            }
        };
        timer1.schedule(timerTask1, 0, 60000);*/

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        String previouslyEncodedImage = preference.getString("image_data", "");



        wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        ll2 = new LinearLayout(this);
        ll21 = new LinearLayout(this);
        ok = new Button(this);
        cancel = new Button(this);
        //et1 = new EditText(this);
        iv= new ImageView(this);
        iv_loc= new ImageView(this);
        tc = new TextClock(this);
        lock = new Button(this);
        tv = new TextView(this);


        l1 = new LinearLayout(this);
        l2 = new LinearLayout(this);
        l3 = new LinearLayout(this);
        l4 = new LinearLayout(this);
        b1 = new Button(this);
        b2 = new Button(this);
        b3 = new Button(this);
        b4 = new Button(this);
        b5 = new Button(this);
        b6 = new Button(this);
        b7 = new Button(this);
        b8 = new Button(this);
        b9 = new Button(this);
        b0 = new Button(this);

        DisplayMetrics dm = getResources().getDisplayMetrics();

        double density = dm.density * 160;
        double x = Math.pow(dm.widthPixels / density, 2);
        double y = Math.pow(dm.heightPixels / density, 2);
        double screenInches = Math.sqrt(x + y);



        int iv_d,ok_d,lock_d,cancel_d,tv_d;
        int btn_l,btn,ll21_d;
        if(dm.heightPixels>=2000){
            iv_d = 320;
            ok_d = 320;
            lock_d = 330;
            cancel_d = 320;
            btn_l = 240;
            btn = 280;
            ll21_d=350;
            tv_d = 120;
        }
        else if(dm.heightPixels>=1920 && dm.heightPixels<2000){
             iv_d = 300;
             ok_d = 220;
             lock_d = 230;
             cancel_d = 220;
             btn_l = 200;
             btn = 220;
             ll21_d=300;
            tv_d = 100;
        }
        else if(dm.heightPixels >1280 && dm.heightPixels<1920){
             iv_d = 220;
             ok_d = 220;
             lock_d = 230;
             cancel_d = 220;
             btn_l = 200;
             btn = 220;
             ll21_d=240;
            tv_d = 80;
        }
        else if(dm.heightPixels>=1000 && dm.heightPixels<=1280){
            iv_d = 150;
            ok_d = 160;
            lock_d = 170;
            cancel_d = 170;
            btn_l = 120;
            btn = 140;
            ll21_d=200;
            tv_d = 60;
        }
        else{
            iv_d = 80;
            ok_d = 100;
            lock_d = 100;
            cancel_d = 100;
            btn_l = 80;
            btn = 100;
            ll21_d=100;
            tv_d = 50;
        }

        final WindowManager.LayoutParams parameters = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                PixelFormat.TRANSLUCENT);
        parameters.gravity = Gravity.CENTER_VERTICAL;



        LinearLayout.LayoutParams llParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
        ll2.setGravity(Gravity.CENTER_HORIZONTAL);
        ll2.setOrientation(LinearLayout.VERTICAL);
        ll2.setLayoutParams(llParameters);

        iv_loc.setLayoutParams(llParameters);
        iv_loc.setScaleType(ImageView.ScaleType.CENTER_CROP);
        //for background setttings
        if( !previouslyEncodedImage.equalsIgnoreCase("") ){
            byte[] b = Base64.decode(previouslyEncodedImage, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);

            BitmapDrawable ob = new BitmapDrawable(getResources(), bitmap);
            ll2.setBackground(ob);
        }
        else{
            WallpaperManager wallpaperManager = WallpaperManager.getInstance(getApplicationContext());
            Drawable wallpaperDrawable = wallpaperManager.getDrawable();
            ll2.setBackground(wallpaperDrawable);

            Bitmap yourSelectedImage = ((BitmapDrawable)wallpaperDrawable).getBitmap();

            yourSelectedImage = scaleCenterCrop(yourSelectedImage,dm.heightPixels,dm.widthPixels);


            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            yourSelectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();

            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            editor.putString("image_data",encodedImage);
            editor.commit();

        }




        LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT ,iv_d );
        params3.setMargins(50, 20, 50, 0);
        iv.setLayoutParams(params3);
        iv.setBackgroundResource(R.drawable.back_logo);

        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT ,WindowManager.LayoutParams.WRAP_CONTENT );
        params1.setMargins(10, 10, 10, 10);
        tc.setLayoutParams(params1);
        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/font11.ttf");
        tc.setTypeface(type);
        tc.setPadding(20,10,20,10);
        tc.setBackgroundResource(R.drawable.rec);
        tc.setGravity(Gravity.CENTER);
        tc.setTextSize(65);
        tc.setTextColor(Color.argb(255, 0, 166, 225));

        LinearLayout.LayoutParams param3 = new LinearLayout.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT ,tv_d );
        param3.setMargins(50, 10, 50, 0);
        tv.setPadding(50,0,50,0);
        tv.setTextColor(Color.WHITE);
        tv.setLayoutParams(param3);
        tv.setGravity(Gravity.CENTER);

        tv.setBackgroundResource(R.drawable.rec);

        if(preference.getString("charging","") == null || preference.getString("charging","").equals("off")){
            tv.setVisibility(View.INVISIBLE);
        }
        else if(preference.getString("charging","").equals("on")){
            tv.setVisibility(View.VISIBLE);
            tv.setText("Charging");
        }



        LinearLayout.LayoutParams params61 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT ,ll21_d);
        params61.setMargins(0, 0, 0, 0);
        ll21.setGravity(Gravity.CENTER);
        ll21.setOrientation(LinearLayout.HORIZONTAL);
        ll21.setLayoutParams(params61);

        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                ok_d ,ok_d);
        params2.setMargins(10, 0, 10, 0);
        ok.setLayoutParams(params2);
        ok.setBackgroundResource(R.drawable.ok);
        ok.getBackground().setAlpha(180);
        ok.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams params22 = new LinearLayout.LayoutParams(
                cancel_d ,cancel_d);
        params22.setMargins(10, 0, 10, 0);
        cancel.setLayoutParams(params22);
        cancel.setBackgroundResource(R.drawable.can);
        cancel.getBackground().setAlpha(180);
        cancel.setGravity(Gravity.CENTER);


        LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(
                lock_d ,lock_d);
        params4.setMargins(10, 0, 10, 0);
        lock.setLayoutParams(params4);
        lock.setBackgroundResource(R.drawable.scr_lock);
        lock.getBackground().setAlpha(180);
        lock.setGravity(Gravity.CENTER);



        //keyboard
        LinearLayout.LayoutParams params6 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT ,btn_l);
        params6.setMargins(0, 10, 0, 10);
        l1.setGravity(Gravity.CENTER);
        l1.setOrientation(LinearLayout.HORIZONTAL);
        l1.setLayoutParams(params6);

        l2.setGravity(Gravity.CENTER);
        l2.setOrientation(LinearLayout.HORIZONTAL);
        l2.setLayoutParams(params6);

        l3.setGravity(Gravity.CENTER);
        l3.setOrientation(LinearLayout.HORIZONTAL);
        l3.setLayoutParams(params6);

        l4.setGravity(Gravity.CENTER);
        l4.setOrientation(LinearLayout.HORIZONTAL);
        l4.setLayoutParams(params6);


        LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams(
                btn ,btn);
        params5.setMargins(20, 10, 20, 10);
        b1.setLayoutParams(params5);
        b1.setBackgroundResource(R.drawable.cir);
        b1.setText("1");
        b1.setTextSize(30);
        b1.setTextColor(Color.argb(255, 255, 255, 225));
        b1.setGravity(Gravity.CENTER);

        b2.setLayoutParams(params5);
        b2.setBackgroundResource(R.drawable.cir);
        b2.setText("2");
        b2.setTextColor(Color.argb(255, 255, 255, 225));
        b2.setGravity(Gravity.CENTER);
        b2.setTextSize(30);

        b3.setLayoutParams(params5);
        b3.setBackgroundResource(R.drawable.cir);
        b3.setText("3");
        b3.setTextColor(Color.argb(255, 255, 255, 225));
        b3.setGravity(Gravity.CENTER);
        b3.setTextSize(30);

        b4.setLayoutParams(params5);
        b4.setBackgroundResource(R.drawable.cir);
        b4.setText("4");
        b4.setTextColor(Color.argb(255, 255, 255, 225));
        b4.setGravity(Gravity.CENTER);
        b4.setTextSize(30);

        b5.setLayoutParams(params5);
        b5.setBackgroundResource(R.drawable.cir);
        b5.setText("5");
        b5.setTextColor(Color.argb(255, 255, 255, 225));
        b5.setGravity(Gravity.CENTER);
        b5.setTextSize(30);

        b6.setLayoutParams(params5);
        b6.setBackgroundResource(R.drawable.cir);
        b6.setText("6");
        b6.setTextColor(Color.argb(255, 255, 255, 225));
        b6.setGravity(Gravity.CENTER);
        b6.setTextSize(30);

        b7.setLayoutParams(params5);
        b7.setBackgroundResource(R.drawable.cir);
        b7.setText("7");
        b7.setTextColor(Color.argb(255, 255, 255, 225));
        b7.setGravity(Gravity.CENTER);
        b7.setTextSize(30);

        b8.setLayoutParams(params5);
        b8.setBackgroundResource(R.drawable.cir);
        b8.setText("8");
        b8.setTextColor(Color.argb(255, 255, 255, 225));
        b8.setGravity(Gravity.CENTER);
        b8.setTextSize(30);

        b9.setLayoutParams(params5);
        b9.setBackgroundResource(R.drawable.cir);
        b9.setText("9");
        b9.setTextColor(Color.argb(255, 255, 255, 225));
        b9.setGravity(Gravity.CENTER);
        b9.setTextSize(30);

        b0.setLayoutParams(params5);
        b0.setBackgroundResource(R.drawable.cir);
        b0.setText("0");
        b0.setTextColor(Color.argb(255, 255, 255, 225));
        b0.setGravity(Gravity.CENTER);
        b0.setTextSize(30);

        wm.addView(ll2,parameters);

        //for setting layout portrait all time

        parameters.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        wm.updateViewLayout(ll2, parameters);

        //ll2.addView(iv_loc);
        ll2.addView(iv);
        ll2.addView(tc);
        ll2.addView(tv);
        ll2.addView(ll21);

        ll21.addView(ok);
        ll21.addView(lock);
        ll21.addView(cancel);

        ll2.addView(l1);
        l1.addView(b1);
        l1.addView(b2);
        l1.addView(b3);

        ll2.addView(l2);
        l2.addView(b4);
        l2.addView(b5);
        l2.addView(b6);

        ll2.addView(l3);
        l3.addView(b7);
        l3.addView(b8);
        l3.addView(b9);

        ll2.addView(l4);
        l4.addView(b0);


       /* Intent dialogIntent = new Intent(getApplicationContext(), behind_scr.class);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(dialogIntent);*/

        final Handler handler = new Handler();
        final Timer timer = new Timer();

        final TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {
                handler.post(new Runnable() {

                    @Override
                    public void run() {

                        if(preference.getString("sound","") == "on"){
                            AudioManager am = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);

                            am.setStreamVolume(
                                    AudioManager.STREAM_MUSIC,
                                    am.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                                    0);
                        }


                    }
                });
            }
        };
        timer.schedule(timerTask, 0, 100);

        ll2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_BACK))
                {
                    Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(160);

                    return true;

                }
                return false;
            }
        });

        b1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        b1.getBackground().setAlpha(150);
                        Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        vib.vibrate(40);

                        break;
                    case MotionEvent.ACTION_UP:
                        b1.getBackground().setAlpha(255);
                        b1.setTextSize(20);
                        b1.setBackgroundResource(R.drawable.cir1);


                        pin = pin.concat("1");

                        if(pin.length() == 30){
                            Vibrator vib1 = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib1.vibrate(840);
                        }
                        else{
                            if(preference.getString("pin","").equals(pin)){

                                vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vib.vibrate(60);


                                remove();
                                editor.putString("count","1");
                                editor.putString("LockScr","0");
                                editor.putString("sound","off");
                                editor.commit();

                                pin="";

                            }
                        }




                        break;
                }
                return false;
            }
        });

        b2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        b2.getBackground().setAlpha(150);
                        Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        vib.vibrate(40);

                        break;
                    case MotionEvent.ACTION_UP:
                        b2.getBackground().setAlpha(255);
                        b2.setTextSize(20);
                        b2.setBackgroundResource(R.drawable.cir1);

                        pin = pin.concat("2");
                        if(pin.length() == 30){
                            Vibrator vib1 = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib1.vibrate(840);
                        }
                        else{
                            if(preference.getString("pin","").equals(pin)){

                                vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vib.vibrate(60);


                                remove();
                                editor.putString("count","1");
                                editor.putString("LockScr","0");
                                editor.putString("sound","off");
                                editor.commit();

                                pin="";

                            }
                        }

                        break;
                }
                return false;
            }
        });
        b3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        b3.getBackground().setAlpha(150);
                        Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        vib.vibrate(40);

                        break;
                    case MotionEvent.ACTION_UP:
                        b3.getBackground().setAlpha(255);
                        b3.setTextSize(20);
                        b3.setBackgroundResource(R.drawable.cir1);

                        pin = pin.concat("3");
                        if(pin.length() == 30){
                            Vibrator vib1 = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib1.vibrate(840);
                        }
                        else{
                            if(preference.getString("pin","").equals(pin)){

                                vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vib.vibrate(60);


                                remove();
                                editor.putString("count","1");
                                editor.putString("LockScr","0");
                                editor.putString("sound","off");
                                editor.commit();

                                pin="";

                            }
                        }
                        break;
                }
                return false;
            }
        });
        b4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        b4.getBackground().setAlpha(150);
                        Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        vib.vibrate(40);

                        break;
                    case MotionEvent.ACTION_UP:
                        b4.getBackground().setAlpha(255);
                        b4.setTextSize(20);
                        b4.setBackgroundResource(R.drawable.cir1);

                        pin = pin.concat("4");
                        if(pin.length() == 30){
                            Vibrator vib1 = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib1.vibrate(840);
                        }
                        else{
                            if(preference.getString("pin","").equals(pin)){

                                vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vib.vibrate(60);


                                remove();
                                editor.putString("count","1");
                                editor.putString("LockScr","0");
                                editor.putString("sound","off");
                                editor.commit();

                                pin="";

                            }
                        }
                        break;
                }
                return false;
            }
        });
        b5.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        b5.getBackground().setAlpha(150);
                        Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        vib.vibrate(40);

                        break;
                    case MotionEvent.ACTION_UP:
                        b5.getBackground().setAlpha(255);
                        b5.setTextSize(20);
                        b5.setBackgroundResource(R.drawable.cir1);

                        pin = pin.concat("5");
                        if(pin.length() == 30){
                            Vibrator vib1 = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib1.vibrate(840);
                        }
                        else{
                            if(preference.getString("pin","").equals(pin)){

                                vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vib.vibrate(60);


                                remove();
                                editor.putString("count","1");
                                editor.putString("LockScr","0");
                                editor.putString("sound","off");
                                editor.commit();

                                pin="";

                            }
                        }
                        break;
                }
                return false;
            }
        });
        b6.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        b6.getBackground().setAlpha(150);
                        Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        vib.vibrate(40);

                        break;
                    case MotionEvent.ACTION_UP:
                        b6.getBackground().setAlpha(255);
                        b6.setTextSize(20);
                        b6.setBackgroundResource(R.drawable.cir1);

                        pin = pin.concat("6");
                        if(pin.length() == 30){
                            Vibrator vib1 = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib1.vibrate(840);
                        }
                        else{
                            if(preference.getString("pin","").equals(pin)){

                                vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vib.vibrate(60);


                                remove();
                                editor.putString("count","1");
                                editor.putString("LockScr","0");
                                editor.putString("sound","off");
                                editor.commit();

                                pin="";

                            }
                        }
                        break;
                }
                return false;
            }
        });
        b7.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        b7.getBackground().setAlpha(150);
                        Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        vib.vibrate(40);

                        break;
                    case MotionEvent.ACTION_UP:
                        b7.getBackground().setAlpha(255);
                        b7.setTextSize(20);
                        b7.setBackgroundResource(R.drawable.cir1);

                        pin = pin.concat("7");
                        if(pin.length() == 30){
                            Vibrator vib1 = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib1.vibrate(840);
                        }
                        else{
                            if(preference.getString("pin","").equals(pin)){

                                vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vib.vibrate(60);


                                remove();
                                editor.putString("count","1");
                                editor.putString("LockScr","0");
                                editor.putString("sound","off");
                                editor.commit();

                                pin="";

                            }
                        }
                        break;
                }
                return false;
            }
        });
        b8.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        b8.getBackground().setAlpha(150);
                        Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        vib.vibrate(40);

                        break;
                    case MotionEvent.ACTION_UP:
                        b8.getBackground().setAlpha(255);
                        b8.setTextSize(20);
                        b8.setBackgroundResource(R.drawable.cir1);

                        pin = pin.concat("8");
                        if(pin.length() == 30){
                            Vibrator vib1 = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib1.vibrate(840);
                        }
                        else{
                            if(preference.getString("pin","").equals(pin)){

                                vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vib.vibrate(60);


                                remove();
                                editor.putString("count","1");
                                editor.putString("LockScr","0");
                                editor.putString("sound","off");
                                editor.commit();

                                pin="";

                            }
                        }
                        break;
                }
                return false;
            }
        });
        b9.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        b9.getBackground().setAlpha(150);
                        Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        vib.vibrate(40);

                        break;
                    case MotionEvent.ACTION_UP:
                        b9.getBackground().setAlpha(255);
                        b9.setTextSize(20);
                        b9.setBackgroundResource(R.drawable.cir1);

                        pin = pin.concat("9");
                        if(pin.length() == 30){
                            Vibrator vib1 = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib1.vibrate(840);
                        }
                        else{
                            if(preference.getString("pin","").equals(pin)){

                                vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vib.vibrate(60);


                                remove();
                                editor.putString("count","1");
                                editor.putString("LockScr","0");
                                editor.putString("sound","off");
                                editor.commit();

                                pin="";

                            }
                        }
                        break;
                }
                return false;
            }
        });
        b0.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        b0.getBackground().setAlpha(150);
                        Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        vib.vibrate(40);

                        break;
                    case MotionEvent.ACTION_UP:
                        b0.getBackground().setAlpha(255);
                        b0.setTextSize(20);
                        b0.setBackgroundResource(R.drawable.cir1);

                        pin = pin.concat("0");
                        if(pin.length() == 30){
                            Vibrator vib1 = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib1.vibrate(840);
                        }
                        else{
                            if(preference.getString("pin","").equals(pin)){

                                vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vib.vibrate(60);

                                remove();
                                editor.putString("count","1");
                                editor.putString("LockScr","0");
                                editor.putString("sound","off");
                                editor.commit();

                                pin="";

                            }
                        }
                        break;
                }
                return false;
            }
        });


        cancel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        cancel.getBackground().setAlpha(180);
                        break;
                    case MotionEvent.ACTION_UP:
                        cancel.getBackground().setAlpha(255);

                        b1.getBackground().setAlpha(255);
                        b1.setTextSize(35);
                        b2.getBackground().setAlpha(255);
                        b2.setTextSize(35);
                        b3.getBackground().setAlpha(255);
                        b3.setTextSize(35);
                        b4.getBackground().setAlpha(255);
                        b4.setTextSize(35);
                        b5.getBackground().setAlpha(255);
                        b5.setTextSize(35);
                        b6.getBackground().setAlpha(255);
                        b6.setTextSize(35);
                        b7.getBackground().setAlpha(255);
                        b7.setTextSize(35);
                        b8.getBackground().setAlpha(255);
                        b8.setTextSize(35);
                        b9.getBackground().setAlpha(255);
                        b9.setTextSize(35);
                        b0.getBackground().setAlpha(255);
                        b0.setTextSize(35);


                        b0.setBackgroundResource(R.drawable.cir);
                        b1.setBackgroundResource(R.drawable.cir);
                        b2.setBackgroundResource(R.drawable.cir);
                        b3.setBackgroundResource(R.drawable.cir);
                        b4.setBackgroundResource(R.drawable.cir);
                        b5.setBackgroundResource(R.drawable.cir);
                        b6.setBackgroundResource(R.drawable.cir);
                        b7.setBackgroundResource(R.drawable.cir);
                        b8.setBackgroundResource(R.drawable.cir);
                        b9.setBackgroundResource(R.drawable.cir);

                        Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        vib.vibrate(80);
                        pin="";

                        break;
                }
                return false;
            }
        });

        ok.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ok.getBackground().setAlpha(180);
                        break;
                    case MotionEvent.ACTION_UP:
                        ok.getBackground().setAlpha(255);




                        if(preference.getString("pin","").equals(pin)){

                            Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib.vibrate(60);
                                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);


                                remove();
                                editor.putString("count","1");
                                editor.commit();

                                pin="";




                            }
                            else{
                                Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vib.vibrate(500);
                                pin="";

                                b1.getBackground().setAlpha(255);
                                b1.setTextSize(35);
                                b2.getBackground().setAlpha(255);
                                b2.setTextSize(35);
                                b3.getBackground().setAlpha(255);
                                b3.setTextSize(35);
                                b4.getBackground().setAlpha(255);
                                b4.setTextSize(35);
                                b5.getBackground().setAlpha(255);
                                b5.setTextSize(35);
                                b6.getBackground().setAlpha(255);
                                b6.setTextSize(35);
                                b7.getBackground().setAlpha(255);
                                b7.setTextSize(35);
                                b8.getBackground().setAlpha(255);
                                b8.setTextSize(35);
                                b9.getBackground().setAlpha(255);
                                b9.setTextSize(35);
                                b0.getBackground().setAlpha(255);
                                b0.setTextSize(35);

                                b0.setBackgroundResource(R.drawable.cir2);
                                b1.setBackgroundResource(R.drawable.cir2);
                                b2.setBackgroundResource(R.drawable.cir2);
                                b3.setBackgroundResource(R.drawable.cir2);
                                b4.setBackgroundResource(R.drawable.cir2);
                                b5.setBackgroundResource(R.drawable.cir2);
                                b6.setBackgroundResource(R.drawable.cir2);
                                b7.setBackgroundResource(R.drawable.cir2);
                                b8.setBackgroundResource(R.drawable.cir2);
                                b9.setBackgroundResource(R.drawable.cir2);

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    b0.setBackgroundResource(R.drawable.cir);
                                    b1.setBackgroundResource(R.drawable.cir);
                                    b2.setBackgroundResource(R.drawable.cir);
                                    b3.setBackgroundResource(R.drawable.cir);
                                    b4.setBackgroundResource(R.drawable.cir);
                                    b5.setBackgroundResource(R.drawable.cir);
                                    b6.setBackgroundResource(R.drawable.cir);
                                    b7.setBackgroundResource(R.drawable.cir);
                                    b8.setBackgroundResource(R.drawable.cir);
                                    b9.setBackgroundResource(R.drawable.cir);
                                }
                            }, 900);


                        }

                        break;
                }
                return false;
            }
        });

        lock.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        lock.getBackground().setAlpha(10);
                        break;
                    case MotionEvent.ACTION_UP:
                        lock.getBackground().setAlpha(255);
                        DevicePolicyManager DPM;
                        ComponentName NM;

                        DPM=(DevicePolicyManager)getApplication().getSystemService(Context.DEVICE_POLICY_SERVICE);
                        NM=new ComponentName(getApplication(),Admin.class);
                        DPM.setPasswordQuality(NM,DevicePolicyManager.PASSWORD_QUALITY_SOMETHING);
                        DPM.lockNow();

                        break;
                }
                return false;
            }
        });



        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public void remove(){
        try{
            try{
                wm.removeView(ll2);
            }
            catch (IllegalArgumentException e){
                stopSelf();
            }

        }
        catch (NullPointerException e){
            stopSelf();
        }

        //
    }
    private void startForeground() {
        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setTicker(getResources().getString(R.string.app_name))// set ticker. you can set a string also .setTicker("your string")
                .setContentText("Lock Screen Running")
                .setSmallIcon(R.drawable.notific)//set your own logo
                .setContentIntent(null)
                .setOngoing(true)
                .build();
        startForeground(999,notification);
    }
    public Bitmap scaleCenterCrop(Bitmap source, int newHeight, int newWidth) {
        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();

        // Compute the scaling factors to fit the new height and width, respectively.
        // To cover the final image, the final scaling will be the bigger
        // of these two.
        float xScale = (float) newWidth / sourceWidth;
        float yScale = (float) newHeight / sourceHeight;
        float scale = Math.max(xScale, yScale);

        // Now get the size of the source bitmap when scaled
        float scaledWidth = scale * sourceWidth;
        float scaledHeight = scale * sourceHeight;

        // Let's find out the upper left coordinates if the scaled bitmap
        // should be centered in the new size give by the parameters
        float left = (newWidth - scaledWidth) / 2;
        float top = (newHeight - scaledHeight) / 2;

        // The target rectangle for the new, scaled version of the source bitmap will now
        // be
        RectF targetRect = new RectF(left, top, left + scaledWidth, top + scaledHeight);

        // Finally, we create a new bitmap of the specified size and draw our new,
        // scaled bitmap onto it.
        Bitmap dest = Bitmap.createBitmap(newWidth, newHeight, source.getConfig());
        Canvas canvas = new Canvas(dest);
        canvas.drawBitmap(source, null, targetRect, null);

        return dest;
    }

    /*public class CallReceiver extends PhonecallReceiver {

        @Override
        protected void onIncomingCallReceived(Context ctx, String number, Date start)
        {
            //
            LockScr.wm.removeView(LockScr.ll2);
        }

        @Override
        protected void onIncomingCallAnswered(Context ctx, String number, Date start)
        {
            //
        }

        @Override
        protected void onIncomingCallEnded(Context ctx, String number, Date start, Date end)
        {
            //
        }

        @Override
        protected void onOutgoingCallStarted(Context ctx, String number, Date start)
        {
            //
        }

        @Override
        protected void onOutgoingCallEnded(Context ctx, String number, Date start, Date end)
        {
            //
        }

        @Override
        protected void onMissedCall(Context ctx, String number, Date start)
        {
            //
        }


    }*/
}
