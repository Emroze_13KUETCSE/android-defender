package com.example.hungers.androiddefender;

import android.app.Service;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextClock;

public class Catch extends Service {
    private WindowManager wm;
    private LinearLayout lla;
    public static String PREFS_NAME="hungers";

    ImageView iv;

  int dispalyX,displayY;

    public Catch() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        lla = new LinearLayout(this);

        iv = new ImageView(this);

        Display display=wm.getDefaultDisplay();
        Point dimension= new Point();
        display.getSize(dimension);

        dispalyX=dimension.x;
        displayY=dimension.y;

        DisplayMetrics dm = getResources().getDisplayMetrics();

        double density = dm.density * 160;
        double x = Math.pow(dm.widthPixels / density, 2);
        double y = Math.pow(dm.heightPixels / density, 2);
        double screenInches = Math.sqrt(x + y);

        double scd=displayY / screenInches;

        int dp_ll;
        int dp_ok;
        if(dm.heightPixels >1280){
            dp_ll = 210;
            dp_ok = 170;
        }
        else if(dm.heightPixels>=1000 && dm.heightPixels<=1280){
            dp_ll = 135;
            dp_ok = 80;
        }
        else{
            dp_ll = 100;
            dp_ok = 50;
        }

        final WindowManager.LayoutParams parameters = new WindowManager.LayoutParams(dp_ll,dp_ll,WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,WindowManager.LayoutParams.FLAG_FULLSCREEN, PixelFormat.TRANSLUCENT);
        parameters.gravity = Gravity.CENTER;

        parameters.x = 0;
        parameters.y = (displayY/2)-200;

        LinearLayout.LayoutParams llParameters = new LinearLayout.LayoutParams(dp_ll,dp_ll);
        lla.setBackgroundResource(R.drawable.circle5); //or whatever your image is
        lla.setGravity(Gravity.CENTER);
        lla.setOrientation(LinearLayout.VERTICAL);
        lla.setLayoutParams(llParameters);


        LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
                dp_ok,dp_ok );
        params3.setMargins(0, 0, 0, 0);
        iv.setLayoutParams(params3);
        iv.setBackgroundResource(R.drawable.can1);

        wm.addView(lla,parameters);
        lla.addView(iv);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        wm.removeView(lla);
    }
}
