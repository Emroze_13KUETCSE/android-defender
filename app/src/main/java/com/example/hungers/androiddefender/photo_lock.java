package com.example.hungers.androiddefender;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.TextClock;

import javax.crypto.spec.SecretKeySpec;

public class photo_lock extends Service {
    public static String PREFS_NAME="hungers";

    private WindowManager wm;
    private RelativeLayout ll2;


    private android.widget.Button ok,cancel;

    private android.widget.Button lock;


    //photo

    protected boolean exactMatch;
    protected final String fileName = "Security_Key";
    protected int count;
    protected ImageView iv;
    protected RelativeLayout.LayoutParams params100;
    protected RelativeLayout rl;


    ImageView lock_screen;
    RelativeLayout pointer;


    public photo_lock() {
    }

    @Override
    public void onCreate() {
        RemoteViews remoteViews = new RemoteViews(getPackageName(),
                R.layout.widget);
        android.support.v4.app.NotificationCompat.Builder mBuilder = new android.support.v4.app.NotificationCompat.Builder(
                this).setSmallIcon(R.drawable.notific).setContent(
                remoteViews);

        Intent resultIntent = new Intent(this, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        stackBuilder.addParentStack(MainActivity.class);

        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.button1, resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        startForeground(999, mBuilder.build());

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        //startService(new Intent(getApplicationContext(),ServiceLock.class));
        /*Intent i = new Intent(getApplicationContext(),behind_scr.class);
        startActivity(i);*/
        //

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        ll2 = new RelativeLayout(this);

        ok = new Button(this);
        cancel = new Button(this);
        //et1 = new EditText(this);

        lock = new Button(this);


        //photo
        lock_screen= new ImageView(this);
        pointer= new RelativeLayout(this);


        final WindowManager.LayoutParams parameters = new WindowManager.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,WindowManager.LayoutParams.FLAG_FULLSCREEN, PixelFormat.TRANSLUCENT);

        final WindowManager.LayoutParams parameters1 = new WindowManager.LayoutParams(100,100,WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,WindowManager.LayoutParams.FLAG_FULLSCREEN, PixelFormat.TRANSLUCENT);




        RelativeLayout.LayoutParams llParameters = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT);
        ll2.setBackgroundResource(R.drawable.lock_pic);
        ll2.setLayoutParams(llParameters);



        final RelativeLayout.LayoutParams params_photo = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT);
        params_photo.setMargins(0, 0, 0, 0);
        lock_screen.setLayoutParams(params_photo);
        lock_screen.setScaleType(ImageView.ScaleType.CENTER_CROP);
        lock_screen.setBackgroundResource(R.drawable.lock_pic);



        wm.addView(ll2,parameters);
        ll2.addView(lock_screen);




        count = 0;
        exactMatch = true;


        final SharedPreferences security = getSharedPreferences(fileName , MODE_PRIVATE);

        byte[] decode = Base64.decode(security.getString("sks","0"),Base64.DEFAULT);
        final SecretKeySpec secks = new SecretKeySpec(decode,0,decode.length,"AES");
        final Checker checker = new Checker(secks,getApplicationContext());

        ll2.setOnTouchListener(new View.OnTouchListener() {

            private WindowManager.LayoutParams update = parameters;

            float touchX, touchY;
            int x, y;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        x = update.x;
                        y = update.y;

                        touchX = event.getRawX();
                        touchY = event.getRawY();

                        String xx=String.valueOf(update.x);
                        String yy=String.valueOf(update.y);

                        editor.putString("xl",xx);
                        editor.putString("yl",yy);
                        editor.commit();

                        break;

                    case MotionEvent.ACTION_MOVE:




                        break;

                    case MotionEvent.ACTION_UP:

                        startService(new Intent(getApplicationContext(),touch.class));

                        break;
                    default:

                        break;
                }
                return false;
            }
        });

        /*lock_screen.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int x = (int) motionEvent.getX();
                int y = (int) motionEvent.getY();

                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if(count < 3){
                            //drawCircle(x,y,rl);
                            params_pointer.setMargins(x-50,y-50,0,0);
                            ll2.addView(pointer);
                            if(!checker.checkFinalX(x,count) || !checker.checkFinalY(y,count)){
                                exactMatch = false;
                            }
                            count++;
                        }

                        if(count >= 3){
                            if(exactMatch){

                                wm.removeView(ll2);
                            }


                            if(!exactMatch){
                                Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vib.vibrate(40);

                                ll2.removeView(pointer);
                            }

                        }
                }

                return true;
            }
        });*/



        return START_STICKY;
    }
    protected void drawCircle(int x, int y,RelativeLayout rl){
        iv = new ImageView(this);
        iv.setImageResource(R.drawable.circle5);
        params100 = new RelativeLayout.LayoutParams(100,100);
        params100.setMargins(x-50,y-50,0,0);
        rl.addView(iv, params100);
    }
    }
