package com.example.hungers.androiddefender;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class change_safe_num extends AppCompatActivity {
    public static String PREFS_NAME="hungers";
    static final int PICK_CONTACT_REQUEST = 1;

    Button contact;
    Button ok;
    EditText pin,new_num;
    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_safe_num);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();
        ok=(Button)findViewById(R.id.btn_ok1);
        pin=(EditText) findViewById(R.id.et_pin1);
        new_num=(EditText) findViewById(R.id.et_pin2);
        tv = (TextView) findViewById(R.id.tv_num);

        tv.setText(preference.getString("safe_number",""));


        contact=(Button) findViewById(R.id.contact1);

        contact.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        contact.getBackground().setAlpha(100);
                        break;
                    case MotionEvent.ACTION_UP:
                        contact.getBackground().setAlpha(255);

                        editor.putString("contact_flag","true");
                        editor.commit();

                        Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
                        pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE); // Show user only contacts w/ phone numbers
                        startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);

                        break;
                }

                return false;
            }
        });

        ok.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ok.getBackground().setAlpha(100);
                        break;
                    case MotionEvent.ACTION_UP:
                        ok.getBackground().setAlpha(255);
                        if (pin.getText().toString().equals("") || pin.getText().toString().equals("")  ) {
                            LayoutInflater inflater = getLayoutInflater();
                            View layout = inflater.inflate(R.layout.toast,
                                    (ViewGroup) findViewById(R.id.toast_layout_root));

                            ImageView image = (ImageView) layout.findViewById(R.id.image);
                            image.setImageResource(R.drawable.logo);
                            TextView text = (TextView) layout.findViewById(R.id.text);
                            text.setText("Text fields are empty!!!");

                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.BOTTOM, 0, 110);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();
                            Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib.vibrate(500);
                        }
                        else {
                            if (!preference.getString("pin", null).equals(pin.getText().toString())) {

                                pin.setText("");
                                new_num.setText("");

                                Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vib.vibrate(500);
                                LayoutInflater inflater = getLayoutInflater();
                                View layout = inflater.inflate(R.layout.toast,
                                        (ViewGroup) findViewById(R.id.toast_layout_root));

                                ImageView image = (ImageView) layout.findViewById(R.id.image);
                                image.setImageResource(R.drawable.logo);
                                TextView text = (TextView) layout.findViewById(R.id.text);
                                text.setText("PIN mis-matched");

                                Toast toast = new Toast(getApplicationContext());
                                toast.setGravity(Gravity.BOTTOM, 0, 110);
                                toast.setDuration(Toast.LENGTH_LONG);
                                toast.setView(layout);
                                toast.show();
                            } else if (preference.getString("pin", null).equals(pin.getText().toString())) {

                                editor.putString("safe_number", new_num.getText().toString());
                                editor.commit();

                                tv.setText(preference.getString("safe_number",""));

                                LayoutInflater inflater = getLayoutInflater();
                                View layout = inflater.inflate(R.layout.toast,
                                        (ViewGroup) findViewById(R.id.toast_layout_root));

                                ImageView image = (ImageView) layout.findViewById(R.id.image);
                                image.setImageResource(R.drawable.logo);
                                TextView text = (TextView) layout.findViewById(R.id.text);
                                text.setText("Safe Number has been changed");

                                Toast toast = new Toast(getApplicationContext());
                                toast.setGravity(Gravity.BOTTOM, 0, 110);
                                toast.setDuration(Toast.LENGTH_LONG);
                                toast.setView(layout);
                                toast.show();
                                finish();
                            }
                        }

                        break;
                }
                return false;
            }
        });

    }
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();



    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request it is that we're responding to
        if (requestCode == PICK_CONTACT_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // Get the URI that points to the selected contact

                Uri contactUri = data.getData();
                String[] projection    = new String[] {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                        ContactsContract.CommonDataKinds.Phone.NUMBER};

                Cursor people = getContentResolver().query(contactUri, projection, null, null, null);
                people.moveToFirst();

                int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

                String name ;
                String number;
                name   = people.getString(indexName);
                number = people.getString(indexNumber);

                SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor=preference.edit();

                editor.putString("contact_number",number);
                editor.putString("contact_name",name);
                editor.commit();

                new_num.setText(number);

                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast,
                        (ViewGroup) findViewById(R.id.toast_layout_root));

                ImageView image = (ImageView) layout.findViewById(R.id.image);
                image.setImageResource(R.drawable.logo);
                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText(preference.getString("contact_number",null).toString());

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.BOTTOM, 0, 110);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
            }
        }
    }
}
